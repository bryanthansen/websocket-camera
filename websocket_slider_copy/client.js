/*
 * Bryant Hansen
 * javascript voting system
 */

var debugCache;
var refreshPeriod = 10; // milliseconds
var host = "anita";

/**
 * code from:
 * http://www.html5gamedevs.com/topic/1828-how-to-calculate-fps-in-plain-javascript/
 */
var fps = {
    startTime : 0,
    frameNumber : 0,
    getFPS : function(){
        this.frameNumber++;
        var d = new Date().getTime(),
            currentTime = ( d - this.startTime ) / 1000,
            result = Math.floor( ( this.frameNumber / currentTime ) );

        if( currentTime > 1 ){
            this.startTime = new Date().getTime();
            this.frameNumber = 0;
        }
        return result;

    }
};

var ws = new WebSocket("ws://" + host + ":8080/websocket");

var poll = function() {
    //if (!this.running) return;
    if (this.running !== undefined) {
        if (!this.running) return;
    }
    console.log("poll")
    setTimeout(function() {
        ws.send("get");
        poll();
    }, refreshPeriod);
};

function toggleRunState() {
    if ((this.running === undefined) || (this.running == true)) {
        console.log("setting running state to false")
        titleH1.innerHTML = "Slider Scale Client (stopped)";
        this.running = false;
    }
    else {
        console.log("toggleRunState: initiating run...")
        location.reload();
    }
}

var divTop = -7.3;
var divBottom = 88.5;
var divScale = (divTop - divBottom) / 10;
ws.onmessage = function (evt) {
    /**
     * The server currently returns a value from 0 to 10, due to the current text on the ruler image
     */
    val = evt.data;
    console.log("server says '%s'", val)
    //console.log("server says '%s', to float = %f, scaled = %f", val, t1, t2)
    this.refreshTextbox.value = fps.getFPS() + " fps";
};

ws.onopen = function() {
    //ws.send("Hello, world");
    this.scaleDiv = document.getElementsByName('scale')[0];
    this.arrowDiv = document.getElementById('arrow');
    this.refreshTextbox = document.getElementsByName('refresh')[0];

    titleH1 = document.getElementById("title");
    titleH1.innerHTML = "Slider Scale Client (running)";

    this.running = true;
    this.arrowDiv.style.top = divBottom + "%"

    poll();
};
