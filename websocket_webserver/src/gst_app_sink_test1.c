    #include <gst/gst.h>
    #include <glib.h>
    #include <gst/app/gstappsink.h>
    #include <stdio.h>
    #include <stdlib.h>

    static gboolean
    bus_call (GstBus     *bus,
              GstMessage *msg,
              gpointer    data)
    {
      GMainLoop *loop = (GMainLoop *) data;

      switch (GST_MESSAGE_TYPE (msg)) {

        case GST_MESSAGE_EOS:
          g_print ("End of stream\n");
          g_main_loop_quit (loop);
          break;

        case GST_MESSAGE_ERROR: {
          gchar  *debug;
          GError *error;

          gst_message_parse_error (msg, &error, &debug);
          g_free (debug);

          g_printerr ("Error: %s\n", error->message);
          g_error_free (error);

          g_main_loop_quit (loop);
          break;
        }
        default:
          g_print("Msg type[%d], Msg type name[%s]\n", GST_MESSAGE_TYPE(msg), GST_MESSAGE_TYPE_NAME(msg));
          break;
      }

      return TRUE;
    }


    static void link_two_elements(GstElement* src_element, GstElement* sink_element)
    {
      if(!gst_element_link(src_element, sink_element))
          g_debug("Error linking %s to %s", gst_element_get_name(src_element), gst_element_get_name(sink_element));
     
    }

    /*
    static void link_two_pads(GstPad* src_pad, GstPad* sink_pad)
    {
      if(!src_pad){
          g_warning("Error: src_pad is NULL on link_two_pads");
          return;
      }

      if(!sink_pad){
          g_warning("Error: sink_pad is NULL on link_two_pads");
          return;
      }

      if(gst_pad_link(src_pad, sink_pad) != GST_PAD_LINK_OK)
          g_debug("Error linking pads %s to %s", gst_pad_get_name(src_pad), gst_pad_get_name(sink_pad));

    }
    */

    static void on_new_buffer (GstElement* object, gpointer user_data)
    {
      FILE* file = (FILE*) user_data;
      GstAppSink* app_sink = (GstAppSink*) object;
      GstMapInfo info;
      GstSample * sample = gst_app_sink_pull_sample(app_sink);
      GstBuffer * buffer = gst_sample_get_buffer(sample);
      gst_buffer_map(buffer, &info, GST_MAP_READ);

      fprintf(stderr, "enter %s\n", __FUNCTION__);

      if(fwrite (info.data, 1 , info.size, file) != info.size)
      {
          g_debug("Error writing data from appsink to file!!!");
      } else {
          g_debug("Data pulled from appsink and writed to file with success!!");
      }
      fprintf(stderr, ".");
    }

    // Pipe to test this src: gst-launch audiotestsrc ! audioconvert ! alawenc ! rtppcmapay ! udpsink host=127.0.0.1 port=5000
    // Equivalent working pipe: gst-launch udpsrc port=5000 caps=application/x-rtp ! gstrtpjitterbuffer ! rtppcmadepay ! alawdec ! audioconvert ! lame ! filesink
    int
    main (int   argc,
          char *argv[])
    {
      GMainLoop *loop;

      GstElement *pipeline, *source, *rtp_jitter, *rtp_alaw_depay,
                 *alaw_decoder, *audio_convert, *lamemp3enc, *appsink, *filesink;
      GstBus* bus;
      GstCaps* udp_caps;
      FILE* appsink_file;
      int udp_port;

      if(argc < 2){
          g_warning("Usage: %s [port_to_be_listened]", argv[0]);
          return -1;
      }

      udp_port = atoi(argv[1]);

      /* Initialisation */
      gst_init (&argc, &argv);

      udp_caps = gst_caps_from_string("application/x-rtp");
      if(!udp_caps){
          g_warning("Error alocating the udp caps");
          return -1;
      }

      loop = g_main_loop_new (NULL, FALSE);

      /* Create gstreamer elements */
      pipeline      = gst_pipeline_new("rtp-mp3-stream-decoder");
      source        = gst_element_factory_make("udpsrc",  "udp-rtp-source");
      rtp_jitter     = gst_element_factory_make("rtpjitterbuffer",  "rtp-jitter-buffer");
      rtp_alaw_depay = gst_element_factory_make("rtppcmadepay",  "rtp_alaw_depay");
      alaw_decoder   = gst_element_factory_make("alawdec","alaw-decoder");
      audio_convert  = gst_element_factory_make("audioconvert","audio-convert");
      lamemp3enc     = gst_element_factory_make("lamemp3enc","mp3-encoder");
      filesink       = gst_element_factory_make("filesink", "file-mp3-output");
      appsink        = gst_element_factory_make("appsink", "sink-buffer");

      if (!pipeline || !source || !rtp_jitter || !filesink ||
          !rtp_alaw_depay || !alaw_decoder || !audio_convert || !lamemp3enc || !appsink) {
          g_printerr ("Elements could not be created. Exiting.\n");
          return -1;
      }

      appsink_file = fopen("received_audio_appsink.mp3", "w");
      if(!appsink_file){
          g_printerr ("Appsink file could not be created. Exiting.\n");
          return -1;
      }

      /* Set up the pipeline */

      /* we set the properties to the source element to receive only rtp packets*/
      g_object_set(G_OBJECT (source), "port", udp_port, NULL);
      g_object_set(G_OBJECT (source), "caps", udp_caps, NULL);
      /* we set the location of the mp3 generated file */
      g_object_set(G_OBJECT (filesink), "location", "received_audio_filesink.mp3", NULL);

      /*
        Make appsink emit the "new-preroll" and "new-buffer" signals. This option is by default disabled because
        signal emission is expensive and unneeded when the application prefers to operate in pull mode.
      */
      gst_app_sink_set_emit_signals ((GstAppSink*) appsink, TRUE);

      /* we add a message handler */
      bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
      gst_bus_add_watch (bus, bus_call, loop);
      gst_object_unref (bus);

      /* we add all elements into the pipeline */
      gst_bin_add_many (GST_BIN (pipeline),
                        source, rtp_jitter, rtp_alaw_depay, alaw_decoder, audio_convert, lamemp3enc, appsink, NULL);

      /* When i just addn the appsink this the example stops to work and the file will be empty (and im not using the appsink, just the filesink)
      if(gst_bin_add(GST_BIN (pipeline), appsink)){
          g_debug("Adcionou appsink com sucesso");
      }else{
          g_debug("Erro ao Adcionar appsink");
      }*/

      /* we link all the elements together */
      link_two_elements(source, rtp_jitter);
      link_two_elements(rtp_jitter, rtp_alaw_depay);
      link_two_elements(rtp_alaw_depay, alaw_decoder);
      link_two_elements(alaw_decoder, audio_convert);
      link_two_elements(audio_convert, lamemp3enc);
      link_two_elements(lamemp3enc, appsink);


      /*
        Appsink (in this case of yours) is meant to *replace* filesink. You don't seem to link appsink to anything and also you link filesink to lame. So, remove filesink from your pipeline, and use appsink in its place.
      */


      /* Conecting to the new-buffer signal emited by the appsink */
      g_signal_connect (appsink, "new-buffer",  G_CALLBACK (on_new_buffer), appsink_file);

      /* Set the pipeline to "playing" state*/
      g_print ("Now listening on port: %d\n", udp_port);
      gst_element_set_state (pipeline, GST_STATE_PLAYING);

      /* Iterate */
      g_print ("Running...\n");
      g_main_loop_run (loop);

      /* Out of the main loop, clean up nicely */
      g_print ("Returned, stopping listening\n");
      gst_element_set_state (pipeline, GST_STATE_NULL);

      g_print ("Deleting pipeline\n");
      gst_object_unref (GST_OBJECT (pipeline));
      fclose(appsink_file);

      return 0;
    }

