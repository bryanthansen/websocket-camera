/**
 * 
 * @author Bryant Hansen
 */

#include <stdio.h>
#include <unistd.h>
#include "gst_v4l_to_png_buf_threaded.h"

int main (int argc, char *argv[]) {
  static struct camera_thread_context * thread_context = NULL;
  int err;

  err = camera_start(argc, argv, &thread_context);
  if (err) {
    fprintf(stderr, "ERROR: camera_start failed, code %d.  Aborting.\n", err);
  }
  else {
    fprintf(stderr, "%s: launch_camera_thread completed, result = %d\n",
            __FUNCTION__, err);

    sleep(30);
    fprintf(stderr, "%s: sleep completed\n", __FUNCTION__);

    err = camera_stop(thread_context);
    if (err) {
      fprintf(stderr, "ERROR: camera_stop failed, code %d.  Aborting.\n", err);
    }
    else {
      fprintf(stderr, "%s: completed join with status of %ld, result = %d\n",
              __FUNCTION__, (long int)thread_context->status, err);
    }
  }
  return err;
}
