/******************************************************************************
 * Copyright (c) 2011 Supercomputing Systems AG
 * All rights reserved.
 *****************************************************************************/

#ifndef _LOG__H
#define _LOG__H

#include <stdio.h>

#define DEFAULT_LOG_LEVEL INFO

#define DEBUG 0
#define INFO  1
#define WARN  2
#define ERROR 3
#define FATAL 4

#define __SHORTFILE__ strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__

#ifndef LOG_LEVEL
#define LOG_LEVEL DEFAULT_LOG_LEVEL
#endif

#define ANSI_COLOR_RED            "\x1b[31m"
#define ANSI_COLOR_GREEN          "\x1b[32m"
#define ANSI_COLOR_YELLOW         "\x1b[33m"
#define ANSI_COLOR_BLUE           "\x1b[34m"
#define ANSI_COLOR_MAGENTA        "\x1b[35m"
#define ANSI_COLOR_CYAN           "\x1b[36m"
#define ANSI_COLOR_WHITE          "\x1b[37m"
#define ANSI_COLOR_BRIGHT_RED     "\x1b[91m"
#define ANSI_COLOR_BRIGHT_GREEN   "\x1b[92m"
#define ANSI_COLOR_BRIGHT_YELLOW  "\x1b[93m"
#define ANSI_COLOR_BRIGHT_BLUE    "\x1b[94m"
#define ANSI_COLOR_BRIGHT_MAGENTA "\x1b[95m"
#define ANSI_COLOR_BRIGHT_CYAN    "\x1b[96m"
#define ANSI_COLOR_BRIGHT_WHITE   "\x1b[97m"
#define ANSI_COLOR_RESET          "\x1b[0m"

#include <string.h>

	#define ERRORMESSAGE(x, args...)   { fprintf(stderr, ANSI_COLOR_RED "ERROR: ");fprintf(stderr, x, ##args);fprintf(stderr, " (in %s() at %s:%u)" ANSI_COLOR_RESET "\n", __FUNCTION__, __SHORTFILE__, __LINE__);fflush(stderr); }
	#define WARNMESSAGE(x, args...)    { fprintf(stderr, ANSI_COLOR_YELLOW "WARN : ");fprintf(stderr, x, ##args);fprintf(stderr, " (in %s() at %s:%u)" ANSI_COLOR_RESET "\n", __FUNCTION__, __SHORTFILE__, __LINE__);fflush(stderr); }
	#define INFOMESSAGE(x, args...)    { fprintf(stderr, "Info : ");fprintf(stderr, x, ##args);fprintf(stderr, " (in %s() at %s:%u)\n", __FUNCTION__, __SHORTFILE__, __LINE__);fflush(stderr); }
  #if LOG_LEVEL==DEBUG
	  #define DEBUGMESSAGE(x, args...) { fprintf(stderr, ANSI_COLOR_BRIGHT_BLUE "debug: ");fprintf(stderr, x, ##args);fprintf(stderr, " (in %s() at %s:%u)" ANSI_COLOR_RESET "\n", __FUNCTION__, __SHORTFILE__, __LINE__);fflush(stderr); }
  #else
    #define DEBUGMESSAGE(args...)
  #endif

	#define HEADERMESSAGE(x, args...)  { fprintf(stderr, ANSI_COLOR_BRIGHT_WHITE "--\n-- "); fprintf(stderr, x, ##args);fprintf(stderr, "\n--" ANSI_COLOR_RESET "\n"); fflush(stderr); }

#endif // _LOG__H
