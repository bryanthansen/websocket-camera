#include <gst/gst.h>
#include <glib.h>


static gboolean bus_call (GstBus *bus, GstMessage *msg, gpointer data)
{
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {

    case GST_MESSAGE_EOS:
      g_print ("End of stream\n");
      g_main_loop_quit (loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar  *debug;
      GError *error;

      gst_message_parse_error (msg, &error, &debug);
      g_free (debug);

      g_printerr ("Error: %s\n", error->message);
      g_error_free (error);

      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}



int main (int argc, char *argv[])
{
  GMainLoop *loop;
  GstElement *pipeline;
  GstBus *bus;
  guint bus_watch_id;
  gchar *descr;
  GError *error = NULL;

  /* Initialisation */
  gst_init (&argc, &argv);
  loop = g_main_loop_new (NULL, FALSE);
  descr =
    /*
     * scaling is a little too challenging for this platform
    g_strdup_printf (
      "v4l2src device=/dev/video0 "
      "! video/x-raw,width=320,height=240,framerate=60/1 "
      "! videoscale ! video/x-raw,width=640,height=480 "
      "! autovideosink"
    );


  descr =
      g_strdup_printf (
            "v4l2src device=/dev/video0 "
            "! decodebin "
            "! timeoverlay "
                "halignment=right "
                "valignment=bottom "
                "text=\"Stream time:\" "
                "shaded-background=true "
                "font-desc=\"Sans, 24\" "
            "! videoconvert "
            "! videoscale "
            "! pngenc compression-level=1 "
            "! queue "
            "! appsink max-buffers=5 name=sink caps=\"" CAPS "\""
      );
  g_print ("parsing pipeline: %s\n", descr);



    */
    g_strdup_printf (
      "v4l2src device=/dev/video0 "
      "! video/x-raw,width=320,height=240,framerate=60/1 "
      "! autovideosink"
    );
  g_print ("parsing pipeline: %s\n", descr);
  pipeline = gst_parse_launch (descr, &error);
  if (error != NULL) {
    g_print ("could not construct pipeline: %s\n", error->message);
    g_clear_error (&error);
    return-1;
  }
  if (!pipeline) {
    g_printerr ("pipeline could not be created. Exiting.\n");
    return -1;
  }

  /* Set up the pipeline */

  /* we add a message handler */
  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
  gst_object_unref (bus);

  /* Set the pipeline to "playing" state*/
  g_print ("Now set pipeline in state playing");
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  /* Iterate */
  g_print ("Running...\n");
  g_main_loop_run (loop);

  /* Out of the main loop, clean up nicely */
  g_print ("Returned, stopping playback\n");
  gst_element_set_state (pipeline, GST_STATE_NULL);

  g_print ("Deleting pipeline\n");
  gst_object_unref (GST_OBJECT (pipeline));
  g_source_remove (bus_watch_id);
  g_main_loop_unref (loop);

  return 0;
}

