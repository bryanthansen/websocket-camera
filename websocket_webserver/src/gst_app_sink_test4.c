/**
    from:
        https://gstreamer.freedesktop.org/documentation/application-development/advanced/pipeline-manipulation.html

*/

#include <gst/gst.h>

#define FRAME_DIR "/tmp/.log"
#define MAX_LEN 120
#define MAX_FILE_LEN (MAX_LEN - strlen(FRAME_DIR))

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <libgen.h>

#include <gst/app/gstappsink.h>

#define CAPS "image/png,width=320,pixel-aspect-ratio=1/1"
//"image/png,width=960,height=540,framerate=1/1"

/** compose the path + filename */
// buffer of 120 chars passed
/// @TODO: sanitize input, output, memory allocation, etc
void gen_filename(char * fpath, char * myname) {
  time_t rawtime;
  char strtime[40];
  char timebuf[26];
  struct tm* tm_info;
  struct timeval tv;
  int millisec;

  /** compose time string with millisec accuracy
   *  @TODO: there must be an easier way (ie. a single function call to do this!
   */
  gettimeofday(&tv, NULL);
  tm_info = localtime(&tv.tv_sec);
  time (&rawtime);
  strftime(timebuf, 26, "%Y%m%d_%H%M%S", tm_info);
  millisec = lrint(tv.tv_usec/1000.0); // Round to nearest millisec
  if (millisec>=1000) { // Allow for rounding up to nearest second
    millisec -=1000;
    tv.tv_sec++;
  }
  sprintf(strtime, "%s_%03d", timebuf, millisec);

  /* compose the path + filename */
  strcpy(fpath, FRAME_DIR);
  strncat(fpath, "/", 1);
  strcat(fpath, basename(myname));
  strncat(fpath, "_", 1);
  strncat(fpath, strtime, strlen(strtime));
  strcat(fpath, ".png");

  fprintf(stderr, "%s: returning %s\n", __FUNCTION__, fpath);
}

GstFlowReturn new_preroll(GstAppSink *appsink, gpointer data) {
  g_print ("Got preroll!\n");
  return GST_FLOW_OK;
}

GstFlowReturn new_sample(GstAppSink *appsink, gpointer data) {
  static int framecount = 0;
  g_print ("Got new_sample!\n");
  framecount++;

  GstSample *sample = gst_app_sink_pull_sample(appsink);
  GstCaps *caps = gst_sample_get_caps(sample);
  GstBuffer *buffer = gst_sample_get_buffer(sample);
  const GstStructure *info = gst_sample_get_info(sample);

  g_print ("%s: size of GstStructure info: %zd\n", __FUNCTION__, sizeof(info));
  //g_print ("%s: type of GstStructure info: %ld\n", __FUNCTION__, info->type);

  GstMapInfo map;
  gst_buffer_map (buffer, &map, GST_MAP_READ);

  g_print ("%s: do something with the buffer\n", __FUNCTION__);

  gst_buffer_unmap(buffer, &map);

  // print dot every 30 frames
  if (framecount%30 == 0) {
    g_print (".");
  }

  // show caps on first frame
  if (framecount == 1) {
    g_print ("%s\n", gst_caps_to_string(caps));
  }

  gst_sample_unref (sample);
  return GST_FLOW_OK;
}

GstFlowReturn app_sink_new_sample(GstAppSink *sink, gpointer user_data) {
  //prog_data* pd = (prog_data*)user_data;

  GstSample* sample = gst_app_sink_pull_sample(sink);
  if(sample == NULL) {
    return GST_FLOW_ERROR;
  }
  g_print ("app_sink_new_sample\n");

  GstBuffer* buffer = gst_sample_get_buffer(sample);
  GstMemory* memory = gst_buffer_get_all_memory(buffer);
  GstMapInfo map_info;
  if(! gst_memory_map(memory, &map_info, GST_MAP_READ)) {
    gst_memory_unref(memory);
    gst_sample_unref(sample);
    return GST_FLOW_ERROR;
  }

  //render using map_info.data
  g_print ("render using map_info.data\n");

  gst_memory_unmap(memory, &map_info);
  gst_memory_unref(memory);
  gst_sample_unref(sample);

  return GST_FLOW_OK;
}

static gboolean my_bus_callback (GstBus *bus, GstMessage *message, gpointer data) {
  g_print ("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));
  switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR: {
      GError *err;
      gchar *debug;

      gst_message_parse_error (message, &err, &debug);
      g_print ("Error: %s\n", err->message);
      g_error_free (err);
      g_free (debug);    
      break;
    }
    case GST_MESSAGE_EOS:
      g_print ("my_bus_callback: end-of-stream\n");
      /* end-of-stream */
      break;
    default:
      /* unhandled message */
      break;
  }
  /* we want to be notified again the next time there is a message
   * on the bus, so returning TRUE (FALSE means we want to stop watching
   * for messages on the bus and our callback should not be called again)
   */
  return TRUE;
}


int main (int argc, char *argv[]) {
  GstElement *pipeline, *sink;
  gint width, height;
  GstSample *sample;
  gchar *descr;
  GError *error = NULL;
  gint64 duration, position;
  GstStateChangeReturn ret;
  gboolean res;
  GstMapInfo map;

  gst_init (&argc, &argv);

  if (argc != 2) {
    g_print ("usage: %s <uri>\n Writes snapshot.png in the current directory\n",
        argv[0]);
    g_print ("example: \n    %s file://${PWD}/test/data/test.mp4\n", argv[0]);
    exit (-1);
  }

  /* create a new pipeline */
  /** @NOTE:  videoconvert and videoscale can go in either order
   *          both are necessary in the current configuration
   */
  /** for v4l */
  /*
  descr = g_strdup_printf ( 
      "v4l2src device=/dev/video0 capture_mode=0 "  // grab from mipi camera
      "! videoconvert "
      "! videoscale "
      "! pngenc compression-level=1 "
      "! queue "
      "! appsink name=sink caps=\"" CAPS "\""
  ); 
  */
  descr =
      g_strdup_printf (
            "uridecodebin uri=%s "
            "! videoconvert "
            "! videoscale "
            "! pngenc compression-level=1 "
            "! queue "
            "! appsink name=sink caps=\"" CAPS "\"",
             argv[1]
      );
  g_print ("parsing pipeline: %s\n", descr);
  pipeline = gst_parse_launch (descr, &error);

  if (error != NULL) {
    g_print ("could not construct pipeline: %s\n", error->message);
    g_clear_error (&error);
    exit (-1);
  }

  /* get sink */
  sink = gst_bin_get_by_name (GST_BIN (pipeline), "sink");

  gst_app_sink_set_emit_signals((GstAppSink*)sink, TRUE);
  gst_app_sink_set_drop((GstAppSink*)sink, TRUE);
  gst_app_sink_set_max_buffers((GstAppSink*)sink, 5);

  GstAppSinkCallbacks callbacks = { NULL, new_preroll, new_sample };
  gst_app_sink_set_callbacks (GST_APP_SINK(sink), &callbacks, NULL, NULL);

  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  GstBus *bus;
  guint bus_watch_id;
  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  bus_watch_id = gst_bus_add_watch (bus, my_bus_callback, NULL);
  g_print ("created new bus watch, id %d\n", bus_watch_id);
  gst_object_unref (bus);

  /* set to PAUSED to make the first frame arrive in the sink */
  ret = gst_element_set_state (pipeline, GST_STATE_PAUSED);
  switch (ret) {
    case GST_STATE_CHANGE_FAILURE:
      g_print ("failed to play the file\n");
      exit (-1);
    case GST_STATE_CHANGE_NO_PREROLL:
      /* for live sources, we need to set the pipeline to PLAYING before we can
       * receive a buffer. We don't do that yet */
      g_print ("live sources not supported yet\n");
      exit (-1);
    default:
      break;
  }

  /* This can block for up to 5 seconds. If your machine is really overloaded,
   * it might time out before the pipeline prerolled and we generate an error. A
   * better way is to run a mainloop and catch errors there. */
  ret = gst_element_get_state (pipeline, NULL, NULL, 5 * GST_SECOND);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    g_print ("failed to play the file\n");
    exit (-1);
  }

  /* get the duration */
  gst_element_query_duration (pipeline, GST_FORMAT_TIME, &duration);

  if (duration != -1)
    /* we have a duration, seek to 5% */
    position = duration * 5 / 100;
  else {
    /* no duration, seek to 1 second, this could EOS */
    position = 1 * GST_SECOND;
  }
  /* seek to the a position in the file. Most files have a black first frame so
   * by seeking to somewhere else we have a bigger chance of getting something
   * more interesting. An optimisation would be to detect black images and then
   * seek a little more */
  /*
    gst_element_seek_simple (pipeline, GST_FORMAT_TIME,
      GST_SEEK_FLAG_KEY_UNIT | GST_SEEK_FLAG_FLUSH, position);
  */

  g_print ("%s: position = %lx\n", __FUNCTION__, position);

  /* get the preroll buffer from appsink, this block untils appsink really
   * prerolls */
  g_signal_emit_by_name (sink, "pull-preroll", &sample, NULL);

  /* if we have a buffer now, which is already converted to a png. It's possible that we
   * don't have a buffer because we went EOS right away or had an error. */
  if (sample) {
    GstBuffer *buffer;
    GstCaps *caps;
    GstStructure *s;

    /* get the snapshot buffer format now. We set the caps on the appsink so
     * that it can only be an rgb buffer. The only thing we have not specified
     * on the caps is the height, which is dependant on the pixel-aspect-ratio
     * of the source material */
    caps = gst_sample_get_caps (sample);
    if (!caps) {
      g_print ("could not get snapshot format\n");
      exit (-1);
    }
    s = gst_caps_get_structure (caps, 0);

    /* we need to get the final caps on the buffer to get the size */
    res = gst_structure_get_int (s, "width", &width);
    res |= gst_structure_get_int (s, "height", &height);
    if (!res) {
      g_print ("could not get snapshot dimension\n");
      exit (-1);
    }

    /* create pixmap from buffer and save, gstreamer video buffers have a stride
      * that is rounded up to the nearest multiple of 4 */
    buffer = gst_sample_get_buffer (sample);
    /* Mapping a buffer can fail (non-readable) */
    if (gst_buffer_map (buffer, &map, GST_MAP_READ)) {
      FILE * outfile;
      char fpath[120];

      GstMemory* memory = gst_buffer_get_all_memory(buffer);
      GstMapInfo map_info;
      if(! gst_memory_map(memory, &map_info, GST_MAP_READ)) {
        gst_memory_unref(memory);
        gst_sample_unref(sample);
        return GST_FLOW_ERROR;
      }

      //render using map_info.data
      g_print ("main: render using map_info.data\n");
      gen_filename(fpath, argv[0]);
      fprintf(stderr, "fpath: %s\n", fpath);
      outfile = fopen(fpath, "wb");
      if (outfile) {
        fwrite(map_info.data, map_info.size, 1, outfile);
      }
      fclose(outfile);

      gst_memory_unmap(memory, &map_info);
      gst_memory_unref(memory);

      /// @TODO: try to get another frame here

      gst_buffer_unmap (buffer, &map);
    }
    gst_sample_unref (sample);
  } else {
    g_print ("could not make snapshot\n");
  }

  gst_element_set_state (GST_ELEMENT (pipeline), GST_STATE_PLAYING);

  /* cleanup and exit */
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);

  exit (0);
}
