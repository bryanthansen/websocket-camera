/**
  from:
        https://stackoverflow.com/questions/30904560/libwebsocket-client-example#34622927
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <libwebsockets.h>

#include "log.h"
#include "base64.h"
#include "gst_v4l_to_png_buf_threaded.h"

#define TX_RX_FRAME_SAMPLE_RATE 60

#define KGRN "\033[0;32;32m"
#define KCYN "\033[0;36m"
#define KRED "\033[0;32;31m"
#define KYEL "\033[1;33m"
#define KMAG "\033[0;35m"
#define KBLU "\033[0;32;34m"
#define KCYN_L "\033[1;36m"
#define RESET "\033[0m"

static int destroy_flag = 0;

static void INT_HANDLER(int signo) {
    destroy_flag = 1;
}

#define FRAME_DIR "/tmp/.log"
#define MAX_LEN 80
#define MAX_FILE_LEN (MAX_LEN - strlen(FRAME_DIR))
#define ROOT_PAGE "./test/data/example.html"

#define IMAGE_DIR "./test/data"
//#define IMAGE_DIR "/tmp/.log"
#define IMAGE_NAME "closedbuf.png"

struct echoserver_user_data {
  /** @brief writeable - is the socket writeable? */
  int writeable;
} *my_user_data;


static int uri_handler(struct lws *wsi, char * requested_uri) {
    int32_t len = strlen(requested_uri);
    //fprintf(stderr, "%s: requested_uri: %s\n", __FUNCTION__, requested_uri);
    fprintf(stderr, "%s: requested_uri length: %d\n", __FUNCTION__, len);
    if (!strncmp(requested_uri, "/", 1)) {
        if (1 == len) {
            fprintf(stderr, "request for root (/)returning example.html\n");
            lws_serve_http_file( wsi, ROOT_PAGE, "text/html", NULL, 0 );
        } else {
            char * ending;
            char cmd[256];
            ending = requested_uri + len - 4;
            if (!strncmp(ending, ".png", 4)) {
                char fpath[MAX_LEN];
                char out_buf[MAX_LEN];
                fprintf(stderr, "requested_uri appears to be an image\n");
                strcpy(fpath, FRAME_DIR);
                strncat(fpath, requested_uri, MAX_FILE_LEN);
                fprintf(stderr, "fpath: %s\n", fpath);
                strcpy(out_buf, FRAME_DIR);
                strcat(out_buf, "/out_buf.png");
                /// @TODO: sanitize this
                if( access(fpath, F_OK) != -1 ) {
                    // file exists; return it
                    fprintf(stderr, "fpath: %s exists; serving file\n", fpath);
                    // copy file first before serving using system call
                    sprintf(cmd, "cp -a '%s' '%s'\n", fpath, out_buf);
                    system(cmd);
                    lws_serve_http_file( wsi, out_buf, "image/png", NULL, 0);
                } else {
                    fprintf(stderr, "file '%s' doesn't exist\n", fpath);
                }
            }
            else {
              fprintf(stderr, "unhandled request: %s\n", requested_uri);
            }
        }
    }
    return 0;
}

/* *
 * websocket_write_back: write the string data to the destination wsi.
 */
int websocket_write_back(struct lws *wsi_in, char *str, int str_size_in, int * writeable)
{
    int n;
    int len;
    unsigned char *out = NULL;

    if (str == NULL || wsi_in == NULL)
        return -1;
    if (str_size_in < 1) 
        len = strlen(str);
    else
        len = str_size_in;

    out = (uint8_t *)malloc(
                        sizeof(unsigned char)
                        * (LWS_SEND_BUFFER_PRE_PADDING
                          + len
                          + LWS_SEND_BUFFER_POST_PADDING
    ));
    //* setup the buffer*/
    memcpy (out + LWS_SEND_BUFFER_PRE_PADDING, str, len);
    //* write out*/
    lwsl_info(KBLU"%s %s (writeable=%d)\n"RESET, __FUNCTION__, str, *writeable);

    /// @TODO: consider using a mutex here; fence it with a variable as an initial test
    *writeable = 0;
    n = lws_write(wsi_in, out + LWS_SEND_BUFFER_PRE_PADDING, len, LWS_WRITE_TEXT);
    *writeable = 1;

    /*
    static int iter = 0;
    if (iter++ % TX_RX_FRAME_SAMPLE_RATE) {
      fprintf(stderr, ".");
    }
    else {
      lwsl_notice(KBLU"%s %s\n"RESET, __FUNCTION__, str);
    }
    */

    //* free the buffer*/
    free(out);
    return n;
}

/* *
 * websocket_write_back_binary: write the string data to the destination wsi.
 * 
 * @TODO: determine how to send the image data through a binary socket on the same port
 */
int websocket_write_back_binary(struct lws *wsi_in, char *str, int str_size_in) 
{
    static int iter = 0;

    if (str == NULL || wsi_in == NULL)
        return -1;

    int n;
    int len;
    unsigned char *out = NULL;

    if (str_size_in < 1) 
        len = strlen(str);
    else
        len = str_size_in;

    out = (unsigned char *)malloc(sizeof(unsigned char) * (LWS_SEND_BUFFER_PRE_PADDING + len + LWS_SEND_BUFFER_POST_PADDING));
    //* setup the buffer*/
    memcpy (out + LWS_SEND_BUFFER_PRE_PADDING, str, len );
    //* write out*/
    n = lws_write(wsi_in, out + LWS_SEND_BUFFER_PRE_PADDING, len, LWS_WRITE_BINARY);

    if (iter++ % TX_RX_FRAME_SAMPLE_RATE) {
      fprintf(stderr, "b");
    }
    else {
      lwsl_notice(KBLU"%s wrote %d bytes of binary data\n"RESET, __FUNCTION__, len);
    }
    //* free the buffer*/
    free(out);

    return n;
}

static void dump_payload(const char * function, char * payload) {
  if (payload) {
    if (strlen(payload) > 0) {
      //lwsl_notice(KYEL"[Main Service] %s payload[0]=%d\n"RESET,
      //            __FUNCTION__, (int)payload[0]);
      //lwsl_notice(KYEL"[Main Service] dump_payload\n"RESET);
      //fprintf(stderr, KYEL"[Main Service] dump_payload\n"RESET);
      /*
      fprintf(stderr, KYEL"  %s: ptr = 0x%X\n"RESET,
              __FUNCTION__, (unsigned long)payload);
      */
      lwsl_notice(KYEL"  %s: %s: ptr = 0x%p\n"RESET,
                  function, __FUNCTION__, payload);
    }
  }
}

static int ws_default_callback(
                         const char * calling_function,
                         struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len)
{

  //fprintf(stderr, "w%d", reason);
  switch (reason) {

    case LWS_CALLBACK_HTTP_FILE_COMPLETION:
      lwsl_notice("%s: LWS_CALLBACK_HTTP_FILE_COMPLETION\n",
                  calling_function);
      break;

    case LWS_CALLBACK_ESTABLISHED:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_ESTABLISHED (payload len: %ld)\n"RESET,
                  calling_function, len);
      //dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_FILTER_HTTP_CONNECTION:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_FILTER_HTTP_CONNECTION (payload len: %ld)\n"RESET,
                  calling_function, len);
      //dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_CLOSED:
      lwsl_notice(KYEL"%s: Client close.\n"RESET, calling_function);
      dump_payload(calling_function, in);
      break;

    //to ignore:
    case LWS_CALLBACK_GET_THREAD_ID:
      break;

    case LWS_CALLBACK_HTTP_BIND_PROTOCOL:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_HTTP_BIND_PROTOCOL (payload len: %ld)\n"RESET,
                  calling_function, len);
      dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_HTTP_DROP_PROTOCOL:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_HTTP_DROP_PROTOCOL (payload len: %ld)\n"RESET,
                  calling_function, len);
      dump_payload(calling_function, in);
      break;


    /** The next few come in rapid-fire
     *    print a single letter to show the flow & order
     */
    case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
      //fprintf(stderr, "C");
      // lwsl_notice(KCYN_L"%s: LWS_CALLBACK_CHANGE_MODE_POLL_FD (payload len: %ld)\n"RESET,
      //             calling_function, len);
      // dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_LOCK_POLL:
      //fprintf(stderr, "L");
      //lwsl_notice(KCYN_L"%s: LWS_CALLBACK_LOCK_POLL (payload len: %ld)\n"RESET,
      //            calling_function, len);
      //dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_UNLOCK_POLL:
      //fprintf(stderr, "U");
      // lwsl_notice(KCYN_L"%s: LWS_CALLBACK_UNLOCK_POLL (payload len: %ld)\n"RESET,
      //            calling_function, len);
      // dump_payload(calling_function, in);
      break;


    case LWS_CALLBACK_FILTER_NETWORK_CONNECTION:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_FILTER_NETWORK_CONNECTION (payload len: %ld)\n"RESET,
                  calling_function, len);
      //dump_payload(calling_function, in);
      fprintf(stderr, "  %s: payload ptr = 0x%p\n", calling_function, in);
      break;

    case LWS_CALLBACK_PROTOCOL_INIT:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_PROTOCOL_INIT (payload len: %ld)\n"RESET,
                  calling_function, len);
      break;

    case LWS_CALLBACK_SERVER_NEW_CLIENT_INSTANTIATED:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_SERVER_NEW_CLIENT_INSTANTIATED (payload len: %ld)\n"RESET,
                  calling_function, len);
      dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_WSI_CREATE:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_WSI_CREATE (payload len: %ld)\n"RESET,
                  calling_function, len);
      dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_ADD_POLL_FD:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_ADD_POLL_FD (payload len: %ld)\n"RESET,
                  calling_function, len);
      dump_payload(calling_function, in);
      break;

    //LWS_CALLBACK_CLOSED_HTTP				=  5,
    case LWS_CALLBACK_CLOSED_HTTP:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_CLOSED_HTTP (payload len: %ld)\n"RESET,
                  calling_function, len);
      dump_payload(calling_function, in);
      break;

    //LWS_CALLBACK_WSI_DESTROY				= 30,
    //WSI_TOKEN_HTTP_RANGE					= 30,
    case LWS_CALLBACK_WSI_DESTROY:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_WSI_DESTROY (payload len: %ld)\n"RESET,
                  calling_function, len);
      dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION (payload len: %ld)\n"RESET,
                  calling_function, len);
      //dump_payload(calling_function, in);
      break;

    case LWS_CALLBACK_DEL_POLL_FD:
      lwsl_notice(KCYN_L"%s: LWS_CALLBACK_DEL_POLL_FD (payload len: %ld)\n"RESET,
                  calling_function, len);
      //dump_payload(calling_function, in);
      break;

    default:
      lwsl_notice("%s: unhandled callback %d)\n", calling_function, reason);
      break;
  }

  return 0;
}

#define MAX_IMAGE_SIZE_BYTES 1000000
#define MAX_RESPONSE_SIZE_BYTES (1024 + MAX_IMAGE_SIZE_BYTES)
static int  websocket_send_response( struct lws *wsi, char * in, const char * function, int * writeable)
{
  char cbResponse[MAX_RESPONSE_SIZE_BYTES];
  char strtime[40];
  char timebuf[26];
  struct tm* tm_info;
  struct timeval tv;
  int millisec;
  static int iter = 0;

  gettimeofday(&tv, NULL);
  tm_info = localtime(&tv.tv_sec);
  strftime(timebuf, 26, "%Y%m%d_%H%M%S", tm_info);
  millisec = round(tv.tv_usec / 1000.0); // Round to nearest millisec
  sprintf(strtime, "%s_%03d", timebuf, millisec);

  iter++;

  lwsl_info(KCYN_L"%s (iter %3d): Server received: %s\n"RESET,
              __FUNCTION__, iter, in);

  //* echo back to client*/
  //* try sending JSON response back */
  if (!strncmp(in, "get", 3)) {
    /* compose JSON response manually; consider a json-c solution at some point */
    snprintf(cbResponse, MAX_RESPONSE_SIZE_BYTES,
            "{ "
            "  \"time\": \"%s\", "
            "  \"function\": \"%s\", "
            "  \"size\": \"%zd\", "
            "  \"payload\": \"%s\" "
            "}",
            strtime, function, 0, in);

    lwsl_info(KCYN_L"%s (iter %3d): Server response: %ld bytes\n"RESET,
                __FUNCTION__, iter, strlen(cbResponse));
    websocket_write_back(wsi, cbResponse, -1, writeable);
  }


  else if (!strncmp(in, "base64_png", 10)) {
    char out_buf[MAX_LEN];
    strcpy(out_buf, FRAME_DIR);
    strcat(out_buf, "/");
    strcat(out_buf, IMAGE_NAME);

    //fprintf(stderr, "\n");
    lwsl_info(KCYN_L"%s (iter %3d): PNG FRAME REQUEST: fetching %s\n"RESET,
                __FUNCTION__, iter, out_buf);

    if( access(out_buf, F_OK) != -1 ) {
      FILE * pngbuf = fopen(out_buf, "r");
      if (pngbuf) {
        fseek(pngbuf, 0L, SEEK_END);
        long imageSize = ftell(pngbuf);
        fseek(pngbuf, 0L, SEEK_SET);
        char * imageBuf = malloc(imageSize + 1);
        fread(imageBuf, imageSize, 1, pngbuf);
        fclose(pngbuf);
        int imageBase64Size = Base64encode_len(imageSize);
        char *imageBase64 = malloc(imageBase64Size);
        int result = Base64encode(imageBase64, imageBuf, imageSize);
        lwsl_info(KCYN_L"%s (iter %3d): Base64encode result: %d\n"RESET,
                    __FUNCTION__, iter, result);
        /* compose JSON response manually; consider a json-c solution at some point */
        snprintf(cbResponse, MAX_RESPONSE_SIZE_BYTES,
                "{ "
                "  \"time\": \"%s\", "
                "  \"function\": \"base64_png\", "
                "  \"size\": \"%zd\", "
                "  \"payload\": \"%s\", "
                "  \"datatype\": \"image/png\", "
                "  \"image\": \"%s\" "
                "}",
                strtime, imageSize, in, imageBase64);
        free(imageBuf);
        free(imageBase64);
      }
      else {
        lwsl_warn(KCYN_L"%s (iter %3d): failed to open %s\n"RESET,
                    __FUNCTION__, iter, out_buf);
      }
    }
    else {
      lwsl_warn(KCYN_L"%s (iter %3d): %s does not exist\n"RESET,
                  __FUNCTION__, iter, out_buf);
    }

    /*
    if (iter % 2) {
      fprintf(stderr, ".");
    }
    else {
      lwsl_notice(KCYN_L"%s (iter %3d): Server received: %s\n"RESET,
                  __FUNCTION__, iter, in);
      lwsl_notice(KCYN_L"%s (iter %3d): Server response: %d bytes\n"RESET,
                  __FUNCTION__, iter, strlen(cbResponse));
    }
    */
    lwsl_info(KCYN_L"%s (iter %3d): Server response: %ld bytes\n"RESET,
                __FUNCTION__, iter, strlen(cbResponse));
    websocket_write_back(wsi, cbResponse, -1, writeable);
    
    
    
    
  }
  else if (!in) {
    lwsl_notice(KCYN_L"%s (iter %3d): request pointer is null\n"RESET,
                __FUNCTION__, iter);
  }
  else if (!strlen(in)) {
    lwsl_notice(KCYN_L"%s (iter %3d): request pointer is a zero-length string\n"RESET,
                __FUNCTION__, iter);
  }
  else {
    lwsl_notice(KCYN_L"%s (iter %3d): unknown request: %s\n"RESET,
                __FUNCTION__, iter, in);
  }

  return 0;
}


struct per_session_data {
  int fd;
  int writeable;
} session_data;

static int ws_service_callback(
                         struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len)
{

    int default_writeable = 1;
    int * writeable;

    if (user) {
      writeable = &((struct per_session_data *)user)->writeable;
    }
    else {
      // set writeable to a pointer containing a 1 in case it hasn't been assigned; don't block by default
      writeable = &default_writeable;
    }

    lwsl_notice(KCYN_L"ENTER %s\n"RESET, __FUNCTION__);

    switch (reason) {

      case WSI_TOKEN_HTTP:
        break;

      case LWS_CALLBACK_FILTER_HTTP_CONNECTION:
        lwsl_notice(KCYN_L"%s: LWS_CALLBACK_FILTER_HTTP_CONNECTION (payload len: %ld)\n"RESET,
                    __FUNCTION__, len);
        fprintf(stderr, "  %s: payload ptr = 0x%p\n", __FUNCTION__, in);
        fprintf(stderr, "  %s: user ptr = 0x%p\n", __FUNCTION__, user);
        fprintf(stderr, "  %s: writeable = %d\n", __FUNCTION__, *writeable);

        if (in && (len > 0)) {
          fprintf(stderr, "  %s: req: %s\n", __FUNCTION__, (char *)in);
        }
        break;

      case LWS_CALLBACK_HTTP:
        lwsl_notice(KCYN_L"%s: LWS_CALLBACK_HTTP (payload len: %ld) user data ptr = %p\n"RESET,
                    __FUNCTION__, len, user);
        fprintf(stderr, "  %s: payload ptr = 0x%p\n", __FUNCTION__, in);
        fprintf(stderr, "  %s: user ptr = 0x%p\n", __FUNCTION__, user);
        fprintf(stderr, "  %s: writeable = %d\n", __FUNCTION__, *writeable);

        if (in && (len > 0)) {
          fprintf(stderr, "  %s: req: %s HANDLER DISABLED\n", __FUNCTION__, (char *)in);
          if (!strncmp((char *)in, "GET ", 4)) {
            char * tok = strtok((char *)in, " ");
            fprintf(stderr, "request starts with GET\n");
            if (tok) {
              uri_handler(wsi, tok + 4);
            } else {
              // no spaces in 'in'; try passing the original string
              uri_handler(wsi, in);
            }
          } else {
            fprintf(stderr, "element requested; string does not start with GET request\n");
            uri_handler(wsi, in);
          }
        }
        break;

      //* If receive a data from client*/
      case LWS_CALLBACK_RECEIVE:
        lwsl_notice(KCYN_L"%s: Server received:%s\n"RESET, __FUNCTION__, (char *)in);
        //* echo back to client*/
        websocket_write_back(wsi ,(char *)in, -1, writeable);
        break;

      default:
        return ws_default_callback(__FUNCTION__, wsi, reason, user, in, len);
    }
    return 0;
}


static int callback_http(
              struct lws *wsi,
              enum lws_callback_reasons reason,
              void *user,
              void *in,
              size_t len)
{
  int default_writeable = 1;
  int * writeable;

  if (user) {
    writeable = &((struct per_session_data *)user)->writeable;
    if (!*writeable) {
      lwsl_info("user data exists, but writeable flag is zero (writeable = %d)\n", *writeable);
    }
  }
  else {
    default_writeable = 1;
    writeable = &default_writeable;
  }
  //fprintf(stderr, "h%d", reason);

  switch( reason ) {
    case LWS_CALLBACK_HTTP:
    case LWS_CALLBACK_HTTP_BODY:
      fprintf(stderr, "  %s: user ptr = 0x%p\n", __FUNCTION__, user);
      fprintf(stderr, "  %s: writeable = %d\n", __FUNCTION__, *writeable);
      if (!in) {
        lwsl_notice("%s: request URI string not provided\n", __FUNCTION__);
        break;
      }
      /// @TODO: evaluate security issues of this
      if (strlen((char *)in) < 1) {
        lwsl_notice("no data at string pointer\n");
        break;
      }
      if (!user) {
        lwsl_notice("%s: user data is null\n", __FUNCTION__);
      }
      if (!*writeable) {
        lwsl_warn("%s: writeable = %d; if the user-data pointer were working properly, this would indicate an operation in progress\n",
                  __FUNCTION__, *writeable);
        // break;
      }
      if (!strncmp((char *)in, "GET ", 4)) {
        char * tok = strtok((char *)in, " ");
        lwsl_notice("request starts with GET\n");
        if (tok) {
          uri_handler(wsi, tok + 4);
        } else {
          uri_handler(wsi, in);
        }
      } else {
        lwsl_notice("%s: element requested; string does not start with GET request\n",
                    __FUNCTION__);
        uri_handler(wsi, in);
      }
      break;

    /** @TODO: don't process new response unless we've received this */
    case LWS_CALLBACK_SERVER_WRITEABLE:
      lwsl_info("  %s: user ptr = 0x%p\n", __FUNCTION__, user);
      *writeable = 1;
      lwsl_info(KCYN_L"%s: LWS_CALLBACK_SERVER_WRITEABLE (payload len: %ld)\n"RESET,
                  __FUNCTION__, len);
      dump_payload(__FUNCTION__, in);
      break;

    case LWS_CALLBACK_GET_THREAD_ID:
      *writeable = 1;
      break;

    //* If receive a data from client*/
    case LWS_CALLBACK_RECEIVE:
      lwsl_info(KCYN_L"%s: Server received: %s. writeable=%d\n"RESET,
                  __FUNCTION__, (char *)in, *writeable);
      websocket_send_response(wsi, in, __FUNCTION__, writeable);
      if (!*writeable) {
        lwsl_notice(KCYN_L"  *** %s WARNING: Server Not writeable.  Consider aborting *** \n"RESET,
                    __FUNCTION__);
        // exit(1);
        break;
      }
      break;

    default:
      return ws_default_callback(__FUNCTION__, wsi, reason, user, in, len);
  }

  // close connection
  // libwebsocket_close_and_free_session(context, wsi, LWS_CLOSE_STATUS_NORMAL);
  return 0;
}


/** @TODO: move this into main function and explicitly-declare variable assignments */
static struct lws_protocols protocols[] =
{
  /* The first protocol must always be the HTTP handler */
  {
    .name = "http-only",   /* name */
    .callback = callback_http, /* callback */
    .per_session_data_size = sizeof(struct per_session_data),  /* No per session data. */
    .rx_buffer_size = 0,             /* max frame size / rx buffer */
    .user = &session_data,
  },
  {
    .name = "my-echo-protocol",
    .callback = ws_service_callback,
    .per_session_data_size = sizeof(struct per_session_data),
    .rx_buffer_size = 0,
    .user = &session_data,
  },
  {
    .name = NULL,
    .callback = NULL,
    .per_session_data_size = 0,
    .rx_buffer_size = 0
  } /* terminator */
};


int main(int argc, char *argv[]) {
  // server url will usd port 8000
  int port = 8000;
  const char *interface = NULL;
  struct lws_context_creation_info info;
  struct lws_context *context;
  // Not using ssl
  const char *cert_path = NULL;
  const char *key_path = NULL;
  // no special options
  int opts = 0;

  static struct camera_thread_context * thread_context = NULL;
  int err;

  fprintf(stderr, "%s: starting %s, num args = %d\n",
          __FUNCTION__, argv[0], argc);

  err = camera_start(argc, argv, &thread_context);
  if (err) {
    fprintf(stderr, "ERROR: camera_start failed, code %d.  Aborting.\n", err);
    exit(3);
  }
  fprintf(stderr, "%s: camera_start completed, result = %d\n",
          __FUNCTION__, err);

  session_data.fd = 666;
  session_data.writeable = 1;

  //* register the signal SIGINT handler */
  struct sigaction act;
  act.sa_handler = INT_HANDLER;
  act.sa_flags = 0;
  sigemptyset(&act.sa_mask);
  sigaction( SIGINT, &act, 0);

  //* setup websocket context info*/
  memset(&info, 0, sizeof info);
  info.port = port;
  info.iface = interface;
  info.protocols = protocols;
  info.ssl_cert_filepath = cert_path;
  info.ssl_private_key_filepath = key_path;
  info.gid = -1;
  info.uid = -1;
  info.options = opts;

  //* create libwebsocket context. */
  context = lws_create_context(&info);
  if (context == NULL) {
    lwsl_notice(KRED"[Main] Websocket context create error.\n"RESET);
    return -1;
  }

  lwsl_notice(KGRN"[Main] Websocket context create success.\n"RESET);

  //* websocket service */
  while ( !destroy_flag ) {
    lws_service(context, 50);
  }

  err = camera_stop(thread_context);
  if (err) {
    fprintf(stderr, "ERROR: camera_stop failed, code %d.  Aborting.\n", err);
  }
  else {
    fprintf(stderr, "%s: completed join with status of %ld, result = %d\n",
            __FUNCTION__, (long int)thread_context->status, err);
  }

  //usleep(10);
  sleep(1);
  lws_context_destroy(context);

  free(my_user_data);

  return 0;
}
