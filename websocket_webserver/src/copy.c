/* copy.c

   Copy the file named argv[1] to a new file named in argv[2].

   originated from: http://man7.org/tlpi/code/online/diff/fileio/copy.c.html
   partially re-written

   WARNING: GPLv3

   trivial to replace/rewrite
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "copy.h"

#ifndef BUF_SIZE        /* Allow "cc -D" to override definition */
#define BUF_SIZE 1024
#endif

int
copy(char * infile, char * outfile)
{
    FILE * inputFd, * outputFd;
    size_t numRead;
    char buf[BUF_SIZE];

    /* Open input and output files */

    inputFd = fopen(infile, "r");
    if (!inputFd) {
        fprintf(stderr, "ERROR opening file %s", infile);
        exit(EXIT_FAILURE);
    }

    outputFd = fopen(outfile, "w");
    if (!outputFd) {
        fprintf(stderr, "ERROR opening file %s", outfile);
        exit(EXIT_FAILURE);
    }

    /* Transfer data until we encounter end of input or an error */

    while ((numRead = fread(buf, BUF_SIZE, 1, inputFd)) > 0)
        if (fwrite(buf, numRead, 1, outputFd) != numRead)
            fprintf(stderr, "couldn't write whole buffer");
    if (numRead == -1)
        fprintf(stderr, "read");

    if (fclose(inputFd) == -1)
        fprintf(stderr, "close input");
    if (fclose(outputFd) == -1)
        fprintf(stderr, "close output");

    exit(EXIT_SUCCESS);
}
