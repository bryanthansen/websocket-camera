/**
  TODO: cite reference

  highly-customized source; started from template
*/

#include <libwebsockets.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#define FRAME_DIR "/tmp/.log"
#define MAX_LEN 80
#define MAX_FILE_LEN (MAX_LEN - strlen(FRAME_DIR))
#define ROOT_PAGE "./test/data/example.html"

static int uri_handler(struct lws *wsi, char * requested_uri) {
    int32_t len = strlen(requested_uri);
    //fprintf(stderr, "%s: requested_uri: %s\n", __FUNCTION__, requested_uri);
    fprintf(stderr, "%s: requested_uri length: %d\n", __FUNCTION__, len);
    if (!strncmp(requested_uri, "/", 1)) {
        if (1 == len) {
            fprintf(stderr, "request for root (/)returning example.html\n");
            lws_serve_http_file( wsi, ROOT_PAGE, "text/html", NULL, 0 );
        } else {
            char * ending;
            char cmd[256];
            ending = requested_uri + len - 4;
            if (!strncmp(ending, ".png", 4)) {
                char fpath[MAX_LEN];
                char out_buf[MAX_LEN];
                fprintf(stderr, "requested_uri appears to be an image\n");
                strcpy(fpath, FRAME_DIR);
                strncat(fpath, requested_uri, MAX_FILE_LEN);
                fprintf(stderr, "fpath: %s\n", fpath);
                strcpy(out_buf, FRAME_DIR);
                strcat(out_buf, "/out_buf.png");
                /// @TODO: sanitize this
                if( access(fpath, F_OK) != -1 ) {
                    // file exists; return it
                    fprintf(stderr, "fpath: %s exists; serving file\n", fpath);
                    // copy file first before serving using system call
                    sprintf(cmd, "cp -a '%s' '%s'\n", fpath, out_buf);
                    system(cmd);
                    lws_serve_http_file( wsi, out_buf, "image/png", NULL, 0);
                } else {
                    fprintf(stderr, "file '%s' doesn't exist\n", fpath);
                }
            }
        }
    }
    return 0;
}

static int callback_http( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len )
{
    fprintf(stderr, ".");
    /*
    fprintf(stderr,
            "%s, wsi: 0x%08X, reason %d; userptr: 0x%08X, inptr=0x%08X, len=%ld\n",
             __FUNCTION__, (unsigned int)wsi, reason, (unsigned int)user, (unsigned int)in, len
    );
    */
    switch( reason ) {
        case LWS_CALLBACK_HTTP:
            fprintf(stderr, "\ncalled LWS_CALLBACK_HTTP\n");
            break;
        case LWS_CALLBACK_HTTP_BODY:
            fprintf(stderr, "\ncalled LWS_CALLBACK_HTTP_BODY\n");
            break;
        case LWS_CALLBACK_HTTP_FILE_COMPLETION:
              fprintf(stderr, "LWS_CALLBACK_HTTP_FILE_COMPLETION\n");
              break;
        default:
            break;
    }


          
    /*
    case LWS_CALLBACK_HTTP_BODY:
          fprintf(stderr, "LWS_CALLBACK_HTTP_BODY; len = %ld\n", len);
          if (len > 0) {
              uint16_t ssize = 80;
              char sample[ssize];
              if (len < ssize) ssize = len;
              strncpy(sample, in, ssize - 1);
              sample[ssize-1] = 0;
              fprintf(stderr, "requested URI: %s\n", sample);
          }
          //fprintf(stderr, "requested URI: %s\n", (char *)in);
          break;

    */
    
    switch( reason ) {
        case LWS_CALLBACK_HTTP:
        case LWS_CALLBACK_HTTP_BODY:
            if (!in) {
                fprintf(stderr, "request URI string not provided\n");
                break;
            }
            /// @TODO: evaluate security issues of this
            if (strlen((char *)in) < 1) {
                fprintf(stderr, "no data at string pointer\n");
                break;
            }
            if (!strncmp((char *)in, "GET ", 4)) {
                char * tok = strtok((char *)in, " ");
                fprintf(stderr, "request starts with GET\n");
                if (tok) {
                    uri_handler(wsi, tok + 4);
                } else {
                    // no spaces in 'in'; try passing the original string
                    uri_handler(wsi, in);
                }
            } else {
                fprintf(stderr, "element requested; string does not start with GET request\n");
                uri_handler(wsi, in);
            }
            break;

        default:
            break;
    }

    // close connection
    // libwebsocket_close_and_free_session(context, wsi, LWS_CLOSE_STATUS_NORMAL);
    return 0;
}

#define EXAMPLE_RX_BUFFER_BYTES (1024)
struct payload
{
    unsigned char data[LWS_SEND_BUFFER_PRE_PADDING + EXAMPLE_RX_BUFFER_BYTES + LWS_SEND_BUFFER_POST_PADDING];
    size_t len;
} received_payload;

static int callback_example( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len )
{
    fprintf(stderr, "\n%s\n", __FUNCTION__);
    switch( reason )
    {
        case LWS_CALLBACK_RECEIVE:
            memcpy( &received_payload.data[LWS_SEND_BUFFER_PRE_PADDING], in, len );
            received_payload.len = len;
            lws_callback_on_writable_all_protocol( lws_get_context( wsi ), lws_get_protocol( wsi ) );
            break;

        case LWS_CALLBACK_SERVER_WRITEABLE:
            lws_write( wsi, &received_payload.data[LWS_SEND_BUFFER_PRE_PADDING], received_payload.len, LWS_WRITE_TEXT );
            break;

        default:
            break;
    }

    return 0;
}

enum protocols
{
    PROTOCOL_HTTP = 0,
    PROTOCOL_EXAMPLE,
    PROTOCOL_COUNT
};

static struct lws_protocols protocols[] =
{
    /* The first protocol must always be the HTTP handler */
    {
        "http-only",   /* name */
        callback_http, /* callback */
        0,             /* No per session data. */
        0,             /* max frame size / rx buffer */
    },
    {
        "example-protocol",
        callback_example,
        0,
        EXAMPLE_RX_BUFFER_BYTES,
    },
    { NULL, NULL, 0, 0 } /* terminator */
};

int main( int argc, char *argv[] )
{
    struct lws_context_creation_info info;
    memset( &info, 0, sizeof(info) );

    info.port = 8000;
    info.protocols = protocols;
    info.gid = -1;
    info.uid = -1;

    struct lws_context *context = lws_create_context( &info );

    while( 1 )
    {
        lws_service( context, /* timeout_ms = */ 1000000 );
    }

    lws_context_destroy( context );

    return 0;
}
