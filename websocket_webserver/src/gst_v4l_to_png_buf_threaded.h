/**
 * Bryant Hansen
 *
 * Simple API for a camera interface
 */

#ifndef _GST_V4L_TO_PNG_BUF_THREADED_H
#define _GST_V4L_TO_PNG_BUF_THREADED_H

#include <stdint.h>
#include <pthread.h>
#include <gst/gst.h>

struct camera_thread_context {
  pthread_t thread;
  pthread_attr_t attr;
  void * status;
  GMainLoop *g_main_loop;
};

int camera_start(int argc, char *argv[], struct camera_thread_context ** context);
int camera_stop(struct camera_thread_context * context);
int camera_get_buffer(struct camera_thread_context * context, uint8_t * buf, uint32_t max_len);

#endif
