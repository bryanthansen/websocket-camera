/**
    from:
        https://gstreamer.freedesktop.org/documentation/application-development/advanced/pipeline-manipulation.html

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <libgen.h>
#include <pthread.h>
#include <unistd.h>

#include <gst/gst.h>
#include <gst/app/gstappsink.h>

#include "gst_v4l_to_png_buf_threaded.h"

#define FRAME_DIR "/tmp/.log"
#define BUF_FILE "/tmp/.log/openbuf.png"
#define COMPLETE_FILE "/tmp/.log/closedbuf.png"

#define CAPS "image/png,width=320,pixel-aspect-ratio=1/1"
//"image/png,width=960,height=540,framerate=1/1"

struct CallbackContext {
  gchar *descr;
  GMainLoop *loop;
  FILE * buf_fd;
  gchar *outfile;
  int framecount;
  GstElement * pipeline;
};


#define LOOP 0
static gboolean
seek_to_time (GstElement *pipeline,
          gint64      time_nanoseconds)
{
  if (!gst_element_seek (pipeline, 1.0, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH,
                         GST_SEEK_TYPE_SET, time_nanoseconds,
                         GST_SEEK_TYPE_NONE, GST_CLOCK_TIME_NONE)) {
    g_print ("Seek failed!\n");
  }
  else {
    if (LOOP) {
      /** @TODO: get elapsed time */
      /** @TODO: reset and loop feature */
      GstState state;
      gst_element_get_state (pipeline, &state, NULL, 20000);
      g_print ("%s end of stream: state = %d\n",
              __FUNCTION__, state);
      if (state != GST_STATE_PLAYING) {
        gst_element_set_state (pipeline, GST_STATE_PLAYING);
        gst_element_get_state (pipeline, &state, NULL, 20000);
        g_print ("%s end of stream: new state = %d\n",
                __FUNCTION__, state);
      }
      return TRUE;
    }
  }
  return FALSE;
}


void end_of_stream(GstAppSink *appsink, gpointer data) {
  struct CallbackContext * ctx = (struct CallbackContext *)data;

  if (LOOP) {
    /** @TODO: get elapsed time */
    /** @TODO: reset and loop feature */
    GstState state;
    g_print ("\n%s end of stream after %d frames; seek to beginning\n",
            __FUNCTION__, ctx->framecount);
    gst_element_get_state (ctx->pipeline, &state, NULL, 20000);
    g_print ("%s end of stream: state = %d\n",
            __FUNCTION__, state);
    if (!seek_to_time(ctx->pipeline, 0)) {
      g_main_loop_quit(ctx->loop);
    }
    gst_element_get_state (ctx->pipeline, &state, NULL, 20000);
    g_print ("%s end of stream: old state = %d\n",
            __FUNCTION__, state);
    gst_element_set_state (ctx->pipeline, GST_STATE_PLAYING);
    gst_element_get_state (ctx->pipeline, &state, NULL, 20000);
    g_print ("%s end of stream: new state = %d\n",
            __FUNCTION__, state);
  }
  else {
    g_main_loop_quit(ctx->loop);
  }
}


GstFlowReturn new_sample(GstAppSink *appsink, gpointer data) {
  struct CallbackContext * ctx = (struct CallbackContext *)data;
  ctx->framecount++;

  GstSample *sample = gst_app_sink_pull_sample(appsink);
  GstCaps *caps = gst_sample_get_caps(sample);
  GstBuffer *buffer = gst_sample_get_buffer(sample);
  const GstStructure *info = gst_sample_get_info(sample);

  if (info) {
    g_print ("%s: type of GstStructure info: name=%d(0x%x), type=%zd(0x%zx)\n",
            __FUNCTION__, info->name, info->name, info->type, info->type);
  }

  // show caps on first frame
  if (ctx->framecount == 1) {
    g_print ("%s init: caps = %s\n", __FUNCTION__, gst_caps_to_string(caps));
    g_print ("%s data = %s\n", __FUNCTION__, ctx->descr);
  }

  // print dot every 30 frames
  if (ctx->framecount % 30 == 0) {
    g_print ("n");
  }

  GstMapInfo map;
  /* Mapping a buffer can fail (non-readable) */
  if (gst_buffer_map (buffer, &map, GST_MAP_READ)) {
    FILE * out_fd;

    GstMemory* memory = gst_buffer_get_all_memory(buffer);
    GstMapInfo map_info;
    if(! gst_memory_map(memory, &map_info, GST_MAP_READ)) {
      gst_memory_unref(memory);
      gst_sample_unref(sample);
      return GST_FLOW_ERROR;
    }

    out_fd = fopen(ctx->outfile, "wb");
    if (out_fd) {
      fwrite(map_info.data, map_info.size, 1, out_fd);
      fclose(out_fd);
    }

    if (ctx->buf_fd) {
      fseek(ctx->buf_fd, 0, SEEK_SET);
      fwrite(map_info.data, map_info.size, 1, ctx->buf_fd);
      fflush(ctx->buf_fd);
    }

    /* cleanup */
    gst_memory_unmap(memory, &map_info);
    gst_memory_unref(memory);
    gst_buffer_unmap(buffer, &map);
  }

  //gst_buffer_unref(buffer);
  //gst_caps_unref(caps);
  gst_sample_unref (sample);
  return GST_FLOW_OK;
}


static gboolean my_bus_callback (GstBus *bus, GstMessage *message, gpointer data) {
  struct CallbackContext * ctx = (struct CallbackContext *)data;
  // g_print ("%s: Got %s message\n", __FUNCTION__, GST_MESSAGE_TYPE_NAME (message));
  switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR: {
      GError *err;
      gchar *debug;

      gst_message_parse_error (message, &err, &debug);
      g_print ("Error: %s\n", err->message);
      g_error_free (err);
      g_free (debug);
      break;
    }
    case GST_MESSAGE_EOS:
      g_print ("my_bus_callback: end-of-stream; attemptinig to reset to beginning\n");
      /* end-of-stream */
      seek_to_time(ctx->pipeline, 0);
      break;
    default:
      /* unhandled message */
      break;
  }
  /* we want to be notified again the next time there is a message
   * on the bus, so returning TRUE (FALSE means we want to stop watching
   * for messages on the bus and our callback should not be called again)
   */
  return TRUE;
}


void * v4l_to_buf(void * loop_ptr) {

  GMainLoop *loop = (GMainLoop *)loop_ptr;
  GstElement *pipeline, *appsink;
  gchar *descr;
  GError *error = NULL;
  struct CallbackContext ctx;
  gchar outfile[] = COMPLETE_FILE;
  int timeoverlay = 0;

  /* create a new pipeline */
   if (timeoverlay) {
     descr = g_strdup_printf (
            "v4l2src device=/dev/video0 "
            "! video/x-raw,width=320,height=240 "
            "! decodebin "
            "! timeoverlay "
                "halignment=right "
                "valignment=bottom "
                "text=\"Stream time:\" "
                "shaded-background=true "
                "font-desc=\"Sans, 24\" "
            "! videoconvert "
            "! videoscale "
            "! pngenc compression-level=1 "
            "! queue "
            "! appsink max-buffers=15 name=sink caps=\"" CAPS "\""
       );
   }
   else {
     descr = g_strdup_printf (
            "v4l2src device=/dev/video0 "
            "! video/x-raw,width=320,height=240,framerate=60/1 "
            "! pngenc compression-level=1 "
            "! queue "
            "! appsink max-buffers=15 name=sink caps=\"" CAPS "\""
       );
  }
  g_print ("parsing pipeline: %s\n", descr);
  pipeline = gst_parse_launch (descr, &error);

  if (error != NULL) {
    g_print ("could not construct pipeline: %s\n", error->message);
    g_clear_error (&error);
    exit (-1);
  }

  /* get sink */
  appsink = gst_bin_get_by_name (GST_BIN (pipeline), "sink");

  gst_app_sink_set_emit_signals((GstAppSink*)appsink, TRUE);
  gst_app_sink_set_drop((GstAppSink*)appsink, TRUE);

  /** @NOTE: this seems ineffective in limiting the number of buffers processed */
  gst_app_sink_set_max_buffers((GstAppSink*)appsink, 2);

  //GstAppSinkCallbacks callbacks = { end_of_stream, NULL, new_sample };
  GstAppSinkCallbacks callbacks;
  callbacks.eos = end_of_stream;
  callbacks.new_preroll = NULL;
  callbacks.new_sample = new_sample;

  ctx.descr = descr;
  ctx.loop = loop;
  ctx.framecount = 0;
  ctx.pipeline = pipeline;
  ctx.buf_fd = fopen(BUF_FILE, "w");
  if (!ctx.buf_fd) {
    g_print ("ERROR: could not open BUF_FILE %s\n", BUF_FILE);
    exit(1);
  }
  ctx.outfile = outfile;
  gst_app_sink_set_callbacks (GST_APP_SINK(appsink), &callbacks, &ctx, NULL);

  /** The bus monitor may not be necessary for this use case, but it seems to function */
  GstBus *bus;
  guint bus_watch_id;
  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  bus_watch_id = gst_bus_add_watch (bus, my_bus_callback, loop);
  g_print ("created new bus watch, id %d\n", bus_watch_id);
  gst_object_unref (bus);

  /* Set the pipeline to "playing" state*/
  g_print ("Set pipeline state to playing (%d)\n", GST_STATE_PLAYING);
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  /* Iterate */
  g_print ("Running...\n");
  g_main_loop_run (loop);
  g_print ("Returned, stopping listening\n");

  /* Out of the main loop, clean up nicely */
  if (ctx.buf_fd) {
    fclose(ctx.buf_fd);
  }
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);

  exit (0);
}

int camera_start(int argc, char *argv[], struct camera_thread_context ** context) {
  int err;
  g_print("%s ENTER\n", __FUNCTION__);

  *context = (struct camera_thread_context *)malloc(sizeof(struct camera_thread_context));

  /// @TODO: determine if any command line options should be provided
  gst_init (&argc, &argv);

  /* Initialize and set thread detached attribute */
  pthread_attr_init(&(*context)->attr);
  pthread_attr_setdetachstate(&(*context)->attr, PTHREAD_CREATE_JOINABLE);

  (*context)->g_main_loop = g_main_loop_new (NULL, FALSE);

  err = pthread_create(&(*context)->thread, NULL, &v4l_to_buf, (*context)->g_main_loop);
  //g_print ("INFO: pthread_create returned %d\n", err);
  if (err) {
    g_print("ERROR: return code from pthread_create() is %d\n", err);
    exit(-1);
  }
  return err;
}

int camera_stop(struct camera_thread_context * context) {
  int rc;
  g_print("%s ENTER\n", __FUNCTION__);
  /* Free attribute and wait for the other threads */
  pthread_attr_destroy(&context->attr);

  g_print("%s:%d mark; attempting to kill main loop\n", __FUNCTION__, __LINE__);
  g_main_loop_quit(context->g_main_loop);

  rc = pthread_join(context->thread, context->status);

  g_print("%s:%d mark; main loop killed\n", __FUNCTION__, __LINE__);
  if (rc) {
    g_print("ERROR: return code from pthread_join() is %d\n", rc);
    exit(-1);
  }
  pthread_exit(NULL);
  g_print("%s EXIT\n", __FUNCTION__);
  return rc;
}


/** 
 * Main example
 */

int __main (int argc, char *argv[]) {
  static struct camera_thread_context * thread_context = NULL;
  int err;

  err = camera_start(argc, argv, &thread_context);
  if (err) {
    g_print("ERROR: camera_start failed, code %d.  Aborting.\n", err);
  }
  else {
    g_print("%s: launch_camera_thread completed, result = %d\n",
            __FUNCTION__, err);

    sleep(30);
    g_print("%s: sleep completed\n", __FUNCTION__);

    err = camera_stop(thread_context);
    if (err) {
      g_print("ERROR: camera_stop failed, code %d.  Aborting.\n", err);
    }
    else {
      g_print("%s: completed join with status of %ld, result = %d\n",
              __FUNCTION__, (long int)thread_context->status, err);
    }
  }
  return err;
}
