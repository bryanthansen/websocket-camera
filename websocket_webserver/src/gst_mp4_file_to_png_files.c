/**
    from:
        https://gstreamer.freedesktop.org/documentation/application-development/advanced/pipeline-manipulation.html

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <libgen.h>

#include <gst/gst.h>
#include <gst/app/gstappsink.h>

#define FRAME_DIR "/tmp/.log"
#define MAX_FRAMES 60

#define CAPS "image/png,width=320,pixel-aspect-ratio=1/1"
//"image/png,width=960,height=540,framerate=1/1"

struct CallbackContext {
  gchar *descr;
  GMainLoop *loop;
};

/** compose the path + filename */
// buffer of 120 chars passed
/// @TODO: sanitize input, output, memory allocation, etc
void gen_filename(char * fpath, char * myname) {
  time_t rawtime;
  char strtime[40];
  char timebuf[26];
  struct tm* tm_info;
  struct timeval tv;
  int millisec;

  /** compose time string with millisec accuracy
   *  @TODO: there must be an easier way (ie. a single function call to do this!
   */
  gettimeofday(&tv, NULL);
  tm_info = localtime(&tv.tv_sec);
  time (&rawtime);
  strftime(timebuf, 26, "%Y%m%d_%H%M%S", tm_info);
  millisec = lrint(tv.tv_usec/1000.0); // Round to nearest millisec
  if (millisec>=1000) { // Allow for rounding up to nearest second
    millisec -=1000;
    tv.tv_sec++;
  }
  sprintf(strtime, "%s_%03d", timebuf, millisec);

  /* compose the path + filename */
  strcpy(fpath, FRAME_DIR);
  strncat(fpath, "/", 1);
  strcat(fpath, basename(myname));
  strncat(fpath, "_", 1);
  strncat(fpath, strtime, strlen(strtime));
  strcat(fpath, ".png");

  //fprintf(stderr, "%s: returning %s\n", __FUNCTION__, fpath);
}

GstFlowReturn new_sample(GstAppSink *appsink, gpointer data) {
  struct CallbackContext * ctx = (struct CallbackContext *)data;
  static int framecount = 0;
  framecount++;

  GstSample *sample = gst_app_sink_pull_sample(appsink);
  GstCaps *caps = gst_sample_get_caps(sample);
  GstBuffer *buffer = gst_sample_get_buffer(sample);
  const GstStructure *info = gst_sample_get_info(sample);

  if (info) {
    // g_print ("%s: size of GstStructure info: %ld\n", __FUNCTION__, sizeof(info));
    g_print ("%s: type of GstStructure info: name=%d(0x%x), type=%zd(0x%zx)\n",
            __FUNCTION__, info->name, info->name, info->type, info->type);
  }

  // show caps on first frame
  if (framecount == 1) {
    g_print ("%s init: caps = %s\n", __FUNCTION__, gst_caps_to_string(caps));
    g_print ("%s data = %s\n", __FUNCTION__, ctx->descr);
  }
  else if (framecount > MAX_FRAMES) {
    /** @TODO: determine how to terminate the stream */
    //gst_buffer_unref(buffer);
    //gst_caps_unref(caps);
    gst_sample_unref(sample);
    g_print("Terminate stream by terminating the main loop\n");
    g_main_loop_quit(ctx->loop);
    return GST_FLOW_ERROR;
  }
  // print dot every 30 frames
  if (framecount%10 == 0) {
    g_print (".");
  }

  GstMapInfo map;
  /* Mapping a buffer can fail (non-readable) */
  if (gst_buffer_map (buffer, &map, GST_MAP_READ)) {
    FILE * outfile;
    char fpath[120];

    GstMemory* memory = gst_buffer_get_all_memory(buffer);
    GstMapInfo map_info;
    if(! gst_memory_map(memory, &map_info, GST_MAP_READ)) {
      gst_memory_unref(memory);
      gst_sample_unref(sample);
      return GST_FLOW_ERROR;
    }

    //render using map_info.data
    gen_filename(fpath, __FILE__);
    fprintf(stderr, "write buffer to %s\n", fpath);
    outfile = fopen(fpath, "wb");
    if (outfile) {
      fwrite(map_info.data, map_info.size, 1, outfile);
    }

    /* cleanup */
    fclose(outfile);
    gst_memory_unmap(memory, &map_info);
    gst_memory_unref(memory);
    gst_buffer_unmap(buffer, &map);
  }

  //gst_buffer_unref(buffer);
  //gst_caps_unref(caps);
  gst_sample_unref (sample);
  return GST_FLOW_OK;
}

int main (int argc, char *argv[]) {
  GstElement *pipeline, *appsink;
  gchar *descr;
  GError *error = NULL;
  GMainLoop *loop;
  struct CallbackContext ctx;

  gst_init (&argc, &argv);

  if (argc != 2) {
    g_print ("usage: %s <uri>\n Writes snapshot.png in the current directory\n",
        argv[0]);
    g_print ("example: \n    %s file://${PWD}/test/data/test.mp4\n", argv[0]);
    exit (-1);
  }

  loop = g_main_loop_new (NULL, FALSE);

  /* create a new pipeline */
  /** @NOTE:  videoconvert and videoscale can go in either order
   *          both are necessary in the current configuration */
  descr =
      g_strdup_printf (
            "uridecodebin uri=%s "
            "! timeoverlay halignment=right valignment=bottom text=\"Stream time:\" shaded-background=true font-desc=\"Sans, 24\" "
            "! videoconvert "
            "! videoscale "
            "! pngenc compression-level=1 "
            "! queue "
            "! appsink max-buffers=5 name=sink caps=\"" CAPS "\"",
             argv[1]
      );
  g_print ("parsing pipeline: %s\n", descr);
  pipeline = gst_parse_launch (descr, &error);

  if (error != NULL) {
    g_print ("could not construct pipeline: %s\n", error->message);
    g_clear_error (&error);
    exit (-1);
  }

  /* get sink */
  appsink = gst_bin_get_by_name (GST_BIN (pipeline), "sink");

  gst_app_sink_set_emit_signals((GstAppSink*)appsink, TRUE);
  gst_app_sink_set_drop((GstAppSink*)appsink, TRUE);

  /** @NOTE: this does not seem to actually limit the number of buffers processed */
  gst_app_sink_set_max_buffers((GstAppSink*)appsink, 2);

  GstAppSinkCallbacks callbacks = { NULL, NULL, new_sample };
  ctx.descr = descr;
  ctx.loop = loop;
  gst_app_sink_set_callbacks (GST_APP_SINK(appsink), &callbacks, &ctx, NULL);

  /* Set the pipeline to "playing" state*/
  g_print ("Set pipeline state to playing (%d)\n", GST_STATE_PLAYING);
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  /* Iterate */
  g_print ("Running...\n");
  g_main_loop_run (loop);

  /* Out of the main loop, clean up nicely */
  g_print ("Returned, stopping listening\n");
  gst_element_set_state (pipeline, GST_STATE_NULL);

  /* cleanup and exit */
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);

  exit (0);
}
