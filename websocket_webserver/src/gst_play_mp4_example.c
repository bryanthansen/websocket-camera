/**
  from: https://gstreamer.freedesktop.org/documentation/application-development/basics/helloworld.html

  To compile the helloworld example, use:

    gcc -Wall helloworld.c -o helloworld $(pkg-config --cflags --libs gstreamer-1.0)
    libtool --mode=link gcc -Wall helloworld.c -o helloworld $(pkg-config --cflags --libs gstreamer-1.0)
*/

/*
src/gst_play_mp4_example.c:57:1: Fehler: »on_pad_added« defined but not used [-Werror=unused-function]
 on_pad_added (GstElement *element,
 ^~~~~~~~~~~~
cc1: Alle Warnungen werden als Fehler behandelt
make: *** [Makefile:89: build/gst_play_mp4_example] Fehler 1
*/
#pragma GCC diagnostic ignored "-Wunused-function"

#include <gst/gst.h>
#include <glib.h>

#define CAPS "image/png,width=320,pixel-aspect-ratio=1/1"

static gboolean bus_call (GstBus     *bus,
          GstMessage *msg,
          gpointer    data)
{
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {

    case GST_MESSAGE_EOS:
      g_print ("End of stream\n");
      g_main_loop_quit (loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar  *debug;
      GError *error;

      gst_message_parse_error (msg, &error, &debug);
      g_free (debug);

      g_printerr ("Error: %s\n", error->message);
      g_error_free (error);

      g_main_loop_quit (loop);
      break;
    }
    case GST_MESSAGE_STREAM_STATUS:
    case GST_MESSAGE_STATE_CHANGED:
    case GST_MESSAGE_LATENCY:
    case GST_MESSAGE_DURATION_CHANGED:
      // we currently get these messages before we get an error
      break;
    default:
      g_print ("%s: unhandled signal %d\n", __FUNCTION__, GST_MESSAGE_TYPE (msg));
      break;
  }

  return TRUE;
}


static void
on_pad_added (GstElement *element,
              GstPad     *pad,
              gpointer    data)
{
  GstPad *sinkpad;
  GstElement *decoder = (GstElement *) data;

  /* We can now link this pad with the vorbis-decoder sink pad */
  g_print ("Dynamic pad created, linking demuxer/decoder\n");

  sinkpad = gst_element_get_static_pad (decoder, "sink");

  gst_pad_link (pad, sinkpad);

  gst_object_unref (sinkpad);
}



int
main (int   argc,
      char *argv[])
{
  GMainLoop *loop;

  GstElement *pipeline, *source, *decoder, *conv, *scale, *pngenc, *gstqueue, *sink;
  GstBus *bus;
  guint bus_watch_id;

  /* Initialisation */
  gst_init (&argc, &argv);

  loop = g_main_loop_new (NULL, FALSE);


  /* Check input arguments */
  if (argc != 2) {
    g_printerr ("Usage: %s <mp4 filename>\n", argv[0]);
    return -1;
  }


  /* Create gstreamer elements */
  pipeline = gst_pipeline_new ("audio-player");
  source   = gst_element_factory_make ("filesrc",       "file-source");
  decoder  = gst_element_factory_make ("decodebin",     "decoder");
  conv     = gst_element_factory_make ("videoconvert",  "converter");
  scale    = gst_element_factory_make ("videoscale",    "scaler");
  pngenc   = gst_element_factory_make ("pngenc",        "encoder");
  gstqueue = gst_element_factory_make ("queue",         "queue");
  sink     = gst_element_factory_make ("appsink",       "app-sink");
  //sink     = gst_element_factory_make ("ximagesink",       "video-window");

  if (!pipeline || !source || !decoder || !conv || !scale || !pngenc || !gstqueue || !sink) {
    g_printerr ("One element could not be created. Exiting.\n");
    return -1;
  }

  /* Set up the pipeline */

  /* we set the input filename to the source element */
  g_object_set (G_OBJECT (source), "location", argv[1], NULL);

  // @FIXME: causes segfault
  //g_object_set (G_OBJECT (sink),   "caps", CAPS, NULL);


  /* we add a message handler */
  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
  gst_object_unref (bus);

  /* we add all elements into the pipeline */
  gst_bin_add_many (GST_BIN (pipeline),
                    source, decoder, conv, scale, pngenc, gstqueue, sink, NULL);

  /* we link the elements together */
  /* file-source -> ogg-demuxer ~> vorbis-decoder -> converter -> alsa-output */
  //gst_element_link (source, demuxer);
  gst_element_link_many (source, decoder, conv, scale, pngenc, gstqueue, sink, NULL);
  //g_signal_connect (demuxer, "pad-added", G_CALLBACK (on_pad_added), decoder);

  /* note that the demuxer will be linked to the decoder dynamically.
     The reason is that Ogg may contain various streams (for example
     audio and video). The source pad(s) will be created at run time,
     by the demuxer when it detects the amount and nature of streams.
     Therefore we connect a callback function which will be executed
     when the "pad-added" is emitted.*/


  /* Set the pipeline to "playing" state*/
  g_print ("Now playing: %s\n", argv[1]);
  gst_element_set_state (pipeline, GST_STATE_PLAYING);


  /* Iterate */
  g_print ("Running...\n");
  g_main_loop_run (loop);


  /* Out of the main loop, clean up nicely */
  g_print ("Returned, stopping playback\n");
  gst_element_set_state (pipeline, GST_STATE_NULL);

  g_print ("Deleting pipeline\n");
  gst_object_unref (GST_OBJECT (pipeline));
  g_source_remove (bus_watch_id);
  g_main_loop_unref (loop);

  return 0;
}

