#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libwebsockets.h>

#define FRAME_DIR "/tmp/.log"
#define MAX_LEN 80
#define MAX_FILE_LEN (MAX_LEN - strlen(FRAME_DIR))
#define ROOT_PAGE "./test/data/example.html"

static int uri_handler(struct lws *wsi, char * requested_uri) {
    int32_t len = strlen(requested_uri);
    fprintf(stderr, "requested_uri: %s\n", requested_uri);
    if (strncmp(requested_uri, "/", 1)) {
        // if (1 == len) {
            fprintf(stderr, "request for root (/)returning %s\n", ROOT_PAGE);
            lws_serve_http_file( wsi, ROOT_PAGE, "text/html", NULL, 0 );
        } else {
            char * ending;
            char cmd[256];
            ending = requested_uri + len - 4;
            if (!strncmp(ending, ".png", 4)) {
                char fpath[MAX_LEN];
                char out_buf[MAX_LEN];
                fprintf(stderr, "requested_uri appears to be an image\n");
                strcpy(fpath, FRAME_DIR);
                strncat(fpath, requested_uri, MAX_FILE_LEN);
                fprintf(stderr, "fpath: %s\n", fpath);
                strcpy(out_buf, FRAME_DIR);
                strcat(out_buf, "/out_buf.png");
                /// @TODO: sanitize this
                if( access(fpath, F_OK) != -1 ) {
                    // file exists; return it
                    fprintf(stderr, "fpath: %s exists; serving file\n", fpath);
                    // copy file first before serving?
                    //copy(fpath, out_buf);
                    sprintf(cmd, "cp -a '%s' '%s'\n", fpath, out_buf);
                    system(cmd);
                    lws_serve_http_file( wsi, fpath, "image/png", NULL, 0);
                } else {
                    fprintf(stderr, "file '%s' doesn't exist\n", fpath);
                }
            }
        //}
    }
    return 0;
}

static int callback_http(struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len)
{
    switch (reason) {
        // http://git.warmcat.com/cgi-bin/cgit/libwebsockets/tree/lib/libwebsockets.h#n260
        case LWS_CALLBACK_CLIENT_WRITEABLE:
            fprintf(stderr, "connection established\n");

        // http://git.warmcat.com/cgi-bin/cgit/libwebsockets/tree/lib/libwebsockets.h#n281
        case LWS_CALLBACK_HTTP: {
            char *requested_uri = (char *) in;
            fprintf(stderr, "requested URI: %s\n", requested_uri);

            if (strcmp(requested_uri, "/") == 0) {
                //void *universal_response = "Hello, World!";
                // http://git.warmcat.com/cgi-bin/cgit/libwebsockets/tree/lib/libwebsockets.h#n597
                //lws_write(wsi, universal_response,
                //                   strlen(universal_response), LWS_WRITE_HTTP);
                /*
                char req[256];
                sprintf(req, "GET /%s", ROOT_PAGE);
                uri_handler(wsi, req);
                */
                fprintf(stderr, "request for root (/).  returning %s\n", ROOT_PAGE);
                lws_serve_http_file( wsi, ROOT_PAGE, "text/html", NULL, 0 );
                break;

            } else {
                // try to get current working directory
                char cwd[1024];
                char *resource_path;

                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    // allocate enough memory for the resource path
                    resource_path = malloc(strlen(cwd) + strlen(requested_uri));

                    // join current working direcotry to the resource path
                    sprintf(resource_path, "%s%s", FRAME_DIR, requested_uri);
                    fprintf(stderr, "resource path: %s\n", resource_path);
                    
                    char *extension = strrchr(resource_path, '.');
                    char *mime;
                    
                    // choose mime type based on the file extension
                    if (extension == NULL) {
                        mime = "text/plain";
                    } else if (strcmp(extension, ".png") == 0) {
                        mime = "image/png";
                    } else if (strcmp(extension, ".jpg") == 0) {
                        mime = "image/jpg";
                    } else if (strcmp(extension, ".gif") == 0) {
                        mime = "image/gif";
                    } else if (strcmp(extension, ".html") == 0) {
                        mime = "text/html";
                    } else if (strcmp(extension, ".css") == 0) {
                        mime = "text/css";
                    } else {
                        mime = "text/plain";
                    }
                    /// @TODO: verify existence
                    uri_handler(wsi, requested_uri);
                    // by default non existing resources return code 400
                    // for more information how this function handles headers
                    // see it's source code
                    // http://git.warmcat.com/cgi-bin/cgit/libwebsockets/tree/lib/parsers.c#n1896
                    
                    /// NOTE: not a websocket operation!
                    fprintf(stderr, "%s: sending %s via lws_serve_http_file\n",
                            __FUNCTION__, resource_path);
                    lws_serve_http_file(wsi, resource_path, mime, NULL, 0);

                }
            }

            // close connection
            if (lws_http_transaction_completed(wsi)) {
              fprintf(stderr, "lws_http_transaction_completed returned true\n");
            }
            break;
        }
        case LWS_CALLBACK_HTTP_FILE_COMPLETION:
              fprintf(stderr, "LWS_CALLBACK_HTTP_FILE_COMPLETION\n");
              break;
        case LWS_CALLBACK_HTTP_BODY:
              fprintf(stderr, "LWS_CALLBACK_HTTP_BODY; len = %zd\n", len);
              if (len > 0) {
                  uint16_t ssize = 80;
                  char sample[ssize];
                  if (len < ssize) ssize = len;
                  strncpy(sample, in, ssize - 1);
                  sample[ssize-1] = 0;
                  fprintf(stderr, "requested URI: %s\n", sample);
              }
              //fprintf(stderr, "requested URI: %s\n", (char *)in);
              break;
        case LWS_CALLBACK_GET_THREAD_ID:
            break;
        default:
            fprintf(stderr, "unhandled callback %d\n", reason);
            break;
    }

    return 0;
}

/* list of supported protocols and callbacks */

static struct lws_protocols protocols[] = {
    /* first protocol must always be HTTP handler */
    
    {
        "http-only",    /* name */
        callback_http,  /* callback */
        0               /* per_session_data_size */
    },
    {
        NULL, NULL, 0   /* End of list */
    }
    
};

int main(void) {
    struct lws_context *context;
    struct lws_context_creation_info info;

    memset( &info, 0, sizeof(info) );
    info.port = 8000;
    info.protocols = protocols;
    info.gid = -1;
    info.uid = -1;

    // create libwebsocket context representing this server
    context = lws_create_context( &info );
    if (context == NULL) {
        fprintf(stderr, "libwebsocket init failed\n");
        return -1;
    }

    printf("starting server...\n");

    // infinite loop, the only option to end this serer is
    // by sending SIGTERM. (CTRL+C)
    while (1) {
        lws_service(context, 50);
        // libwebsocket_service will process all waiting events with their
        // callback functions and then wait 50 ms.
        // (this is single threaded webserver and this will keep
        // our server from generating load while there are not
        // requests to process)
    }

    lws_context_destroy(context);

    return 0;
}
