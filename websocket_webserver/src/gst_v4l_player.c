/**
    from:
        https://gstreamer.freedesktop.org/documentation/application-development/advanced/pipeline-manipulation.html

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <libgen.h>

#include <gst/gst.h>

#define CAPS "image/png,width=320,pixel-aspect-ratio=1/1"
#define LOOP 0

int main (int argc, char *argv[]) {
  GstElement *pipeline;
  gchar *descr;
  GError *error = NULL;
  GMainLoop *loop;

  gst_init (&argc, &argv);

  if (argc != 1) {
    g_print ("ERROR: this program takes no arguments\n");
    g_print ("usage: %s\n", argv[0]);
    exit (-1);
  }

  loop = g_main_loop_new (NULL, FALSE);

  /* create a new pipeline */
  /** @NOTE:  videoconvert and videoscale can go in either order
   *          both are necessary in the current configuration */
  //            "multifilesrc location=\"%s\" index=0 stop-index=2 "
  descr =
      g_strdup_printf (
            "v4l2src device=/dev/video0 "
            /*
            "! 'video/x-raw, width=(int)320, height=(int)240,framerate=20/1' "
            "! autovideosink"
            */
            "! video/x-raw,width=320,height=240 "
            "! decodebin "
            "! timeoverlay "
                "text=\"Stream time:\" "
            "! videoconvert "
            "! videoscale "
            "! pngenc compression-level=1 "
            "! queue "
            "! autovideosink"
      );
  g_print ("parsing pipeline: %s\n", descr);
  pipeline = gst_parse_launch (descr, &error);

  if (error != NULL) {
    g_print ("could not construct pipeline: %s\n", error->message);
    g_clear_error (&error);
    exit (-1);
  }

  /* Set the pipeline to "playing" state*/
  g_print ("Set pipeline state to playing (%d)\n", GST_STATE_PLAYING);
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  /* Iterate */
  g_print ("Running...\n");
  g_main_loop_run (loop);
  g_print ("Returned, stopping listening\n");

  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);

  exit (0);
}
