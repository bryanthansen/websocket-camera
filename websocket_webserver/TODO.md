

 * Create small samples of mp3 & ogg with sine wave, or sine wave sweep
   testaudiosrc ! lame ! filesink location="./test/data/test1.mp3"
   testaudiosrc ! oggenc ! filesink location="./test/data/test1.ogg"

 * Create huffyuv sample
   testvideosrc ! huffyuv encoder ! filesink location="out.huffyuv"

 * SVG streaming
   * Live plots streaming

 * Canvas drawing

 * Actual websocket usage

 * Review Bottle

 * Check QT (websocket interface)
