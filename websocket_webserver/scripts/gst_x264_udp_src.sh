
# ERROR: current results:
# WARNUNG: Fehlerhafte Leitung: v4l2src0 konnte nicht mit x264enc0 verknüpft werden mit den Großbuchstaben video/x-raw, width=(int)640, height=(int)480

HOST=192.168.12.106
PORT=5001
GST_DEBUG=4 \
gst-launch-1.0 v4l2src ! \
 video/x-raw,width=640,height=480 ! \
 x264enc tune=zerolatency byte-stream=true \
 bitrate=3000 threads=2 ! \
 h264parse config-interval=1 ! \
 rtph264pay ! udpsink host=$HOST port=$PORT
