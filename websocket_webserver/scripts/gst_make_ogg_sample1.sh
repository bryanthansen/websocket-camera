#!/bin/bash
# Bryant Hansen

#  wave                : Oscillator waveform
#                        flags: lesbar, schreibbar, regelbar
#                        Enum "GstAudioTestSrcWave" Default: 0, "sine"
#                           (0): sine             - Sine
#                           (1): square           - Square
#                           (2): saw              - Saw
#                           (3): triangle         - Triangle
#                           (4): silence          - Silence
#                           (5): white-noise      - White uniform noise
#                           (6): pink-noise       - Pink noise
#                           (7): sine-table       - Sine table
#                           (8): ticks            - Periodic Ticks
#                           (9): gaussian-noise   - White Gaussian noise
#                           (10): red-noise        - Red (brownian) noise
#                           (11): blue-noise       - Blue noise
#                           (12): violet-noise     - Violet noise

#  volume              : Volume of test signal
#                        flags: lesbar, schreibbar, regelbar
#                        Double. Range:               0 -               1 Default:             0.8 


# Cool test:
# gst-launch-1.0 audiotestsrc wave=3 freq=200 num-buffers=1000 ! queue ! audioconvert ! libvisual_lv_scope ! queue ! videoconvert ! videorate ! "video/x-raw,framerate=5/1" ! autovideosink


OUTFILE=./test/data/test1.ogg
[[ "$1" ]] && OUTFILE="$1"
#gst-launch-1.0 audiotestsrc wave=2 freq=200 num-buffers=100 ! queue ! audioconvert ! lamemp3enc target=quality quality=2 ! id3v2mux ! filesink location=$OUTFILE
gst-launch-1.0 audiotestsrc wave=0 freq=512 num-buffers=60 volume=0.4 \
    ! queue \
    ! audioconvert \
    ! vorbisenc \
    ! oggmux \
    ! filesink location="$OUTFILE"
