#!/bin/bash

# from http://stackoverflow.com/questions/15625046/ddg#17330958 via duckduckgo
#gst-launch v4l2src device=/dev/video1 ! video/x-raw-yuv,framerate=30/1 ! queue ! ffmpegcolorspace ! pngenc ! multifilesink location="frame%d.png"

gst-launch-1.0 \
    filesrc location="test/data/test.mp4" num-buffers=5 \
    ! video/x-raw,width=640,height=480,framerate=30/1 \
    ! queue \
    ! pngenc \
    ! multifilesink location="/tmp/.log/$(basename "$0")_%04d.png"
