#!/bin/bash
# Bryant Hansen

# Simple player from /dev/video0
#gst-launch-1.0 -v v4l2src ! 'video/x-raw, width=(int)640, height=(int)480,framerate=20/1' ! autovideosink

#           "! timeoverlay "
#                "halignment=right "
#                "valignment=bottom "
#                "text=\"Stream time:\" "
#                "shaded-background=true "
#                "font-desc=\"Sans, 24\" "

DISPLAY=:0 gst-launch-1.0 -v v4l2src \
! 'video/x-raw, width=(int)320, height=(int)240,framerate=2/1' \
! timeoverlay \
    halignment=right \
    valignment=bottom \
    text="Stream time:" \
    shaded-background=true \
    font-desc="Sans, 12" \
! videoconvert \
! autovideosink
