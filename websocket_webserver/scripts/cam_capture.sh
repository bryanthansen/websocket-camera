#!/bin/bash
# Bryant Hansen

LOGDIR=/tmp/.log
[[ -d "$LOGDIR" ]] || mkdir -p "$LOGDIR"

function dump_frameset() {
    gst-launch-1.0 v4l2src device="/dev/video0" num-buffers=30 \
        ! videoflip method=rotate-180 \
        ! videoconvert \
        ! pngenc compression-level=9 \
        ! multifilesink post-messages=true location="${LOGDIR}/frame_test05_%03d.png"
}

function dump_frame() {
    videodev=/dev/video0
    location="${LOGDIR}/frame_test11.png"
    GST_DEBUG=4 \
    gst-launch-1.0 v4l2src device="$videodev" num-buffers=1 \
        ! videorate \
        ! video/x-raw,framerate=1/1 \
        ! videoflip method=rotate-180 \
        ! videoconvert \
        ! pngenc snapshot=true compression-level=9 \
        ! queue \
        ! multifilesink location="$location" post-messages=true max-files=40 max-file-size=10000000
}

for n in {1..2} ; do
    time dump_frame
    sleep 1
done

exit 0
