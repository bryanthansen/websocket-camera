#!/bin/bash
USAGE="$0 file"

if [[ ! "$1" ]] ; then
    echo "USAGE: $USAGE" >&2
    exit 1
fi
if [[ ! -f "$1" ]] ; then
    echo "$1 does not exist as a file" >&2
    exit 2
fi

gst-launch-1.0 filesrc location="$1" !decodebin name=decoder \
       decoder. ! queue ! videoconvert ! autovideosink \
       decoder. ! queue ! audioconvert ! autoaudiosink
