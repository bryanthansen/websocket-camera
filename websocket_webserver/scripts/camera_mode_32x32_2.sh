#!/bin/sh

# GST_DEBUG=4 gst-launch-1.0 v4l2src \

SCALE_SIZE=240
#SCALE_SIZE=320

test1(){
gst-launch-1.0 v4l2src \
! video/x-raw,format=RGB \
! videoconvert \
! videoscale \
! video/x-raw,width=${SCALE_SIZE},height=${SCALE_SIZE} \
! autovideosink
}

test2(){
gst-launch-1.0 v4l2src \
! video/x-raw \
! videoconvert \
! videoscale \
! video/x-raw,width=${SCALE_SIZE},height=${SCALE_SIZE} \
! autovideosink
}

test2
