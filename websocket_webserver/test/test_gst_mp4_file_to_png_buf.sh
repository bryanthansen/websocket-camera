#!/bin/bash
# Bryant Hansen

bn="$(basename "$0")"
tn="$bn"
tn="${tn#test_}"
tn="${tn%.sh}"
tf=${PWD}/test/data/test.mp4
turi=file://${tf}
conf="./test/test.conf"

[[ -f ./test/functions.sh ]] && . ./test/functions.sh
[[ -f "$conf" ]] && . "$conf"

outdir=/tmp/.log
[[ -d "$outdir" ]] || mkdir -p "$outdir"

printf "$0 ${tf}\n" >&2
if ./build/"$tn" ${tf} ; then
    printf "$tn succeeded.  outfile: %s\n" "$outfile"
    ls -1t "$outdir" | head -n 2 | while read l ; do
        ls -lht "${outdir}/${l}"
    done
    #printf "Files in %s:\n" "$outdir"
    #ls -Alht "$outdir" | head -n 5
else
    ret=$?
    printf "ERROR: $tn failed, code %d\n" $ret
    exit $ret
fi

