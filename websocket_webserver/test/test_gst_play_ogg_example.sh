#!/bin/bash
# Bryant Hansen

bn="$(basename "$0")"
tn="$bn"
tn="${tn#test_}"
tn="${tn%.sh}"
tf=./test/data/test1.ogg
conf="./test/test.conf"

[[ -f ./test/functions.sh ]] && . ./test/functions.sh
[[ -f "$conf" ]] && . "$conf"

printf "$0 $tf\n" >&2
./build/"$tn" "$tf"

