## For the GStreamer experiments

### Install via Gentoo Package Manager
emerge -uavND \
\
media-libs/gst-plugins-base \
media-libs/gst-plugins-good \
media-libs/gst-plugins-ugly \
media-plugins/gst-plugins-flac \
media-plugins/gst-plugins-libav \
media-plugins/gst-plugins-libpng \
media-plugins/gst-plugins-openh264 \
media-plugins/gst-plugins-pulse \
media-plugins/gst-plugins-shout2 \
media-plugins/gst-plugins-taglib \
media-plugins/gst-plugins-uvch264 \
media-plugins/gst-plugins-v4l2 \
media-plugins/gst-plugins-wavpack \
media-plugins/gst-plugins-x264 \
media-plugins/gst-plugins-ximagesrc \
